define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init();

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'ranking/attractdetail/index',
                pk: 'attractID',
                sortName: 'attractID',
                columns: [
                    [
                        {checkbox: true},
                        //{field: 'userID',title:'用户ID',operate:false},
                        {field: 'userID', title:'userID',events: Controller.api.events.userID, formatter: Controller.api.formatter.userID,visible:false},

                        {field: 'rank', title:'序号',operate:false},
                        {field: 'type', title: '收益类型', searchList: {1:'会员管理收益',2:'商家管理收益',3:'创客区域收益',4:'创客管理收益',5:'商家收益'}},
                        {field: 'fees', title: '资产收益（元）',formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'time', title:'交易时间',formatter: Table.api.formatter.datetime,operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"'},
                        {field: 'source', title: '查看详情', formatter: Controller.api.formatter.source,operate:false}
                    ]
                ],
                showToggle: false,
                showColumns: false,
                //是否启用通用搜索
                commonSearch: true,
                //是否启用快速搜索
                search: false,
                //是否始终显示搜索表单
                searchFormVisible: true,
                //all导出整个表,basic导出当前分页
                exportDataType: "all",
                exportTypes: [ 'csv','excel'],
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                userID: function (value, row, index) {
                    console.log(1);
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },
                source: function (value, row, index) {
                    //这里手动构造URL
                    url = "tuan/earningsdetails/index?" + 'source' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="label label-success addtabsit" title="' + '查看分润详情' + '">' + '查看详情' + '</a>';
                }
            },
            events: {//绑定事件的方法
                userID: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-ip': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='userID']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                        Toastr.info("执行了自定义搜索操作");
                    }
                },
                browser: {
                    'click .btn-browser': function (e, value, row, index) {
                        e.stopPropagation();
                        Layer.alert("该行数据为: <code>" + JSON.stringify(row) + "</code>");
                    }
                },
            },
            /*formatter: {
                source: function (value, row, index) {
                    //这里手动构造URL
                    url = "tuan/earningsdetails/index?" + 'source' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="label label-success addtabsit" title="' + '查看分润详情' + '">' + '查看详情' + '</a>';
                }
            }*/
        }
    };
    return Controller;
});