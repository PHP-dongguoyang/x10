define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init();

            //绑定事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                //移除绑定的事件
                $(this).unbind('shown.bs.tab');
            });

            //必须默认触发shown.bs.tab事件
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        table: {
            first: function () {
                // 表格1
                var table1 = $("#table1");
                table1.bootstrapTable({
                    url: 'ranking/consume/table1',
                    toolbar: '#toolbar1',
                    pk: 'consumeID',
                    sortName: 'consumeID',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'rank', title: '排名',operate:false},
                            {field: 'account', title: __('Account')},
                            {field: 'fees', title: __('Fees'),operate:false},
                            {field: 'phone', title: __('Phone'),operate:false},
                            {field: 'time', title: __('Time'),formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',visible: false,},
                            {field: 'userID', title: __('Userid'), operate:false,formatter: Controller.api.formatter.userID},
                        ]
                    ]
                });

                // 为表格1绑定事件
                Table.api.bindevent(table1);
            },
            second: function () {
                // 表格2
                var table2 = $("#table2");
                table2.bootstrapTable({
                    url: 'ranking/consume/table2',
                    toolbar: '#toolbar2',
                    sortName: 'id',
                    search: false,//快速搜索
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    columns: [
                        [
                            {field: 'rank', title: '排行',operate: false},
                            {field: 'account', title: __('商家账号'), operate: 'LIKE %...%', placeholder: '商家账号搜索'},
                            {field: 'cate', title: __('行业'),operate: false},
                            {field: 'shop_name', title: __('商家名称'),operate: false},
                            {field: 'fees', title: __('店铺收益（元）'),operate: false},
                            {field: 'user_num', title: __('绑定会员数'),operate: false},
                            {field: 'phone', title: __('绑定手机号'),operate: 'LIKE %...%', placeholder: '手机号搜索'},
                            {field: 'city', title: __('地区'),operate: false},
                            {field: 'c.time', title: __('时间'), formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',visible: false,},
                            {field: 'shop_id', title:'查看订单交易',operate:false, formatter: Controller.api.formatter.shop_id}
                        ]
                    ]
                });

                // 为表格2绑定事件
                Table.api.bindevent(table2);
            },
            three: function () {
                // 表格3
                var table3 = $("#table3");
                table3.bootstrapTable({
                    url: 'ranking/consume/table3',
                    toolbar: '#toolbar3',
                    // sortName: 'id',
                    search: false,//快速搜索
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'rank', title: '排名',operate:false},
                            {field: 'time', title:'选择时间区域',formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',visible: false,},
                            {field: 'account', title: '会员ID'},
                            {field: 'phone', title: '绑定手机号'},
                            {field: 'fees', title: '总资产收益（元）',operate:false,formatter: Table.api.formatter.get_money},
                            {field: 'userID', title:'查看详情',operate:false, formatter: Controller.api.formatter.userID3}
                        ]
                    ]
                });

                // 为表格3绑定事件
                Table.api.bindevent(table3);
            },
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                userID: function (value, row, index) {

                    //这里手动构造URL
                    url = "tuan/ordertransactionrecord/index?" + "user_id=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="会员消费详情">' + '查看详情' + '</a>';

                },
                shop_id: function (value, row, index) {

                    //这里手动构造URL
                    url = "shopmoney/shoporder/index?" + "shop_id=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="查看订单交易">' + '查看订单交易' + '</a>';

                },
                userID3: function (value, row, index) {
                    //这里手动构造URL
                    url = "ranking/attractdetail/index?" + 'userID' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="' + '资产收益详情' + '">' + '查看详情' + '</a>';
                }
            }
        }
    };
    return Controller;
});