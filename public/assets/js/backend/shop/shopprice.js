define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/shopprice/index',
                    add_url: 'shop/shopprice/add',
                    edit_url: 'shop/shopprice/edit',
                    del_url: 'shop/shopprice/del',
                    multi_url: 'shop/shopprice/multi',
                    table: 'property',

                }
            });

            var table = $("#table");

            //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
                //这里可以对params值进行修改,从而影响搜索条件
                var province_id,city_id,area_id;
                var filter = JSON.parse(params.filter);
                var op = JSON.parse(params.op);
                province_id = $('select[name="province_id"]').val();
                city_id = $('select[name="city_id"]').val();
                area_id = $('select[name="area_id"]').val();
                if(province_id){
                    filter['shop.province_id'] = province_id;
                    op['shop.province_id'] = '=';
                }else{
                    delete filter['shop.province_id'];
                    delete op['shop.province_id'];
                }
                if(city_id){
                    filter['shop.city_id'] = city_id;
                    op['shop.city_id'] = '=';
                }else{
                    delete filter['shop.city_id'];
                    delete op['shop.city_id'];
                }
                if(area_id){
                    filter['shop.area_id'] = area_id;
                    op['shop.area_id'] = '=';
                }else{
                    delete filter['shop.area_id'];
                    delete op['shop.area_id'];
                }
                params.filter = JSON.stringify(filter);
                params.op = JSON.stringify(op);

            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'shop_id',
                sortName: 'shop_id',
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '店铺结算列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col >=9 && col <= 12){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                },
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'shop_id', title: __('商铺ID'),operate: false},
                        {field: 'shop_name', title: __('店铺名称'),operate:'LIKE', placeholder: '关键字，模糊搜索'},
                        {field: 'user.account', title: __('账号信息'),operate:'LIKE', placeholder: '关键字，模糊搜索'},
                        {field: 'user.phone', title: __('绑定手机号'),operate:'LIKE', placeholder: '关键字，模糊搜索'},
                        {field: 'user_num', title: __('绑定会员数'),operate:false},
                        {field: 'city', title: __('地区'),operate: false},
                        {field: 'cate_name', title: __('行业'),operate: false},
                        {field: 'cate_id', title: __('行业'),visible:false,searchList: $.getJSON('shop/shopprice/cate_list')},
                        {field: 'rate', title: __('税率(%)'),operate: false},
                        {field: 'settlement_interval', title: __('提现周期(天)'),operate: false},
                        {field: 'return_money', title: __('返现金额(元)'),operate: false},
                        {field: 'grand_total', title: __('店铺累计收益(元)'),operate: false},
                        {field: 'balance', title: __('账户余额(元)'),operate: false},
                        {field: 'draw_money', title: __('提现总额(元)'),operate: false},
                        {field: 'user_id', title: __('操作'), formatter: Controller.api.formatter.userID,operate: false},
                        {field: 'shop_id', title: __('操作'), formatter: Controller.api.formatter.shop_id,operate: false},
                        {field: 'shop_id', title: __('操作'), formatter: Controller.api.formatter.shop_id3,operate: false},
                        {field: 'area_id', title: __('省市区'), searchList: function () {
                            return Template('categorytpl', {});
                        },visible:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                userID: function (value, row, index) {

                    //这里手动构造URL
                    url = "shop/shoppricedetails/index?" + "userID=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="提现记录">' + '提现记录' + '</a>';

                },
                shop_id: function (value, row, index) {

                    //这里手动构造URL
                    url = "tuan/cashbackrecord/index?" + "shop_id=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="返现记录">' + '返现记录' + '</a>';

                },
                shop_id3: function (value, row, index) {

                    //这里手动构造URL
                    url = "shopmoney/shoporder/index?" + "shop_id=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="交易记录">' + '交易记录' + '</a>';

                }
            },
        }
    };
    return Controller;
});