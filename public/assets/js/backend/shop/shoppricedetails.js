define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/shoppricedetails/index',
                    add_url: 'shop/shoppricedetails/add',
                    edit_url: 'shop/shoppricedetails/edit',
                    del_url: 'shop/shoppricedetails/del',
                    multi_url: 'shop/shoppricedetails/multi',
                    table: 'tags',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'depositlistID',
                sortName: 'depositlistID',
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '店铺提现列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col == 2){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                },
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'userID', title: __('userID'),events: Controller.api.events.userID, formatter: Controller.api.formatter.userID,visible:false},
                        {field: 'depositlistID', title: __('Id'),operate: false},
                        {field: 'serial_number', title: __('提现流水号'),operate: false},
                        {field: 'return_money', title: __('提现金额'),operate: false},
                        // {field: 'balance', title: __('提现后余额'),operate: false},
                        {field: 'time', title: __('提现申请时间'),formatter: Table.api.formatter.datetime,
                            operate: false,
                            addclass: 'datetimerange'},
                        {field: 'audit_time', title: __('申请通过时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table,buttons: [
                            {name: 'detail', text: '提现详情', title: '提现详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'shop/shoppricedetails/edit'}
                        ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                user_id: function (value, row, index) {
                    console.log(1);
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },

            },
            events: {//绑定事件的方法
                userID: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-ip': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='userID']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                        Toastr.info("执行了自定义搜索操作");
                    }
                },
                browser: {
                    'click .btn-browser': function (e, value, row, index) {
                        e.stopPropagation();
                        Layer.alert("该行数据为: <code>" + JSON.stringify(row) + "</code>");
                    }
                },
            }
        }
    };
    return Controller;
});