define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/shopearnings/index',
                    add_url: 'shop/shopearnings/add',
                    edit_url: 'shop/shopearnings/edit',
                    del_url: 'shop/shopearnings/del',
                    multi_url: 'shop/shopearnings/multi',
                    table: 'property',

                }
            });

            var table = $("#table");

            //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
                //这里可以对params值进行修改,从而影响搜索条件
                var province_id,city_id;
                var filter = JSON.parse(params.filter);
                var op = JSON.parse(params.op);
                province_id = $('select[name="province_id"]').val();
                city_id = $('select[name="city_id"]').val();
                //area_id = $('select[name="area_id"]').val();
                if(province_id){
                    filter['province_id'] = province_id;
                    op['province_id'] = '=';
                }else{
                    delete filter['province_id'];
                }
                if(city_id){
                    filter['city_id'] = city_id;
                    op['city_id'] = '=';
                }else{
                    delete filter['city_id'];
                }
                //if(!area_id){
                    delete filter['area_id'];
                //}
                params.filter = JSON.stringify(filter);
                params.op = JSON.stringify(op);

            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'shop_id',
                sortName: 'shop_id',
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel'],
                pagination:false,

                columns: [
                    [
                        {checkbox: true},
                        {field: 'ranking', title: __('排名'),operate: false},
                        {field: 'area_id', title: __('省市'), searchList: function () {
                                return Template('categorytpl', {});
                            },visible:false},
                        {field: 'spp', title: __('店铺累交易量(元)'),operate: false},
                        // {field: 'time', title: __('Time'),formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD"',visible: false,},

                        //启用时间段搜索
                        {
                            field: 'time',
                            title: __('时间选择'),
                            sortable: true,
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            visible: false,
                        },
                        {field: 'name', title: __('城市名称'),operate: false},
                        {field: 'phone', title: __('手机号'),operate: false},
                        // {field: 'time', title: __('Time'),formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',visible: false},
                        {field: 'city_id', title: __('操作'), formatter: Controller.api.formatter.city_id,operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                city_id: function (value, row, index) {
                    //这里手动构造URL\
                    //console.log(row);
                    if (row.type==1){
                        url = "company/rank/top?" + "city_id=" + value;
                    }else{
                        url = "company/rank/agency?" + "city_id=" + value;
                    }
                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="趋势图">' + '趋势图' + '</a>';

                },
            },
        }
    };
    return Controller;
});