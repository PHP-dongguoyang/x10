define(['jquery', 'bootstrap', 'backend', 'addtabs', 'table', 'echarts', 'echarts-theme', 'template','form'], function ($, undefined, Backend, Datatable, Table, Echarts, undefined, Template,Form) {

    var Controller = {
        top: function () {
            $('button[type=reset]').on('click',function(){
                $('input[name=start_date]').remove();
                $('input[name=end_date]').remove();

            });

            var form = $('form');
            var city_id = $('input[name=city_id]').val();
            var url = '/admin/company/rank/get_echart_data?city_id='+city_id;
            var city_name = $('input[name=city_name]').val();
            //提交表单的方法，在表单完成验证后进行提交

            // // 基于准备好的dom，初始化echarts实例
            // var myChart = Echarts.init(document.getElementById('echart'));
            //
            // // 指定图表的配置项和数据
            // var option = {
            //     title: {
            //         text: '交易额排行'
            //     },
            //     tooltip: {},
            //     legend: {
            //         data:['交易额']
            //     },
            //     xAxis: {
            //         data: []
            //     },
            //     yAxis: {},
            //     series: [{
            //         name: '交易额',
            //         type: 'bar',
            //         data: []
            //     }]
            // };
            //
            // // 使用刚指定的配置项和数据显示图表。
            // myChart.setOption(option);
            //
            var data = JSON.parse($('input[name=data]').val());
            // option.xAxis.data = data.name;
            // option.series[0]['data'] = data.spp;
            // myChart.setOption(option);


            // 基于准备好的dom，初始化echarts实例
            var myChart1 = Echarts.init(document.getElementById('echart_amount'), 'walden');
            // 指定图表的配置项和数据
            var option1 =  {
                title: {
                    text: '市级交易额与成交量',
                    subtext: city_name
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                legend: {
                    data: ['交易额', '成交量']
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                // toolbox: {
                //     show: false,
                //     feature: {
                //         magicType: {show: true, type: ['stack', 'tiled']},
                //         saveAsImage: {show: true}
                //     }
                // },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    // data: Orderdata.column
                    data: []
                },
                // yAxis: {
                //
                // },
                // grid: [{
                //     left: 'left',
                //     top: 'top',
                //     right: '35',
                //     bottom: 30
                // }],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                series: [{
                    name: '交易额',
                    type: 'line',
                    smooth: true,
                    areaStyle: {
                        normal: {
                        }
                    },
                    lineStyle: {
                        normal: {
                            width: 1.5
                        }
                    },
                    // data: Orderdata.paydata
                    data:[]
                },
                    {
                        name: '成交量',
                        type: 'line',
                        smooth: true,
                        areaStyle: {
                            normal: {
                            }
                        },
                        lineStyle: {
                            normal: {
                                width: 1.5
                            }
                        },
                        // data: Orderdata.createdata
                        data: []
                    }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart1.setOption(option1);
            $.get(url).done(function(res){
                if(res.code != 1){
                    return;
                }
                var data = JSON.parse(res.data);
                var order_data = data['order'];
                option1.xAxis.data = order_data.date_arr;
                option1.series[0]['data'] = order_data.amount_arr;
                option1.series[1]['data'] = order_data.count_arr;

                myChart1.setOption(option1);

            })



            var myChart2 = Echarts.init(document.getElementById('echart_pie'));
            var pie_data = [];
            for(var i in data.name){
                var pie = {value:data.spp[i] ,name:data.name[i]}
                pie_data.push(pie);
            }
            var option2 = {
                title : {
                    text: '交易额占比',
                    subtext: city_name,
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: data.name
                },
                series : [
                    {
                        name: '交易额',
                        type: 'pie',
                        radius : '55%',
                        center: ['55%', '60%'],
                        data:pie_data,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };

            myChart2.setOption(option2);

            form.attr('action',url);

            Form.api.bindevent(form,function(data,ret){
                // var data = res.data;
                var data = JSON.parse(data);
                var rank_data = JSON.parse(data['rank']['json_data']);
                var order_data = data['order'];

                // option.xAxis.data = rank_data.name;
                // option.series[0]['data'] = rank_data.spp;
                // myChart.setOption(option); //柱状图
                var pie_data = [];
                for(var i in rank_data.name){
                    var pie = {value:rank_data.spp[i] ,name:rank_data.name[i]}
                    pie_data.push(pie);
                }
                option2.series[0]['data'] = pie_data;
                option2.legend.data = rank_data.name;
                myChart2.setOption(option2); //饼图

                option1.xAxis.data = order_data.date_arr;
                option1.series[0]['data'] = order_data.amount_arr;
                option1.series[1]['data'] = order_data.count_arr;
                myChart1.setOption(option1); //走势图
            });
            $(window).resize(function () {
                // myChart.resize();
                myChart1.resize();
                myChart2.resize();
            });


        },

        agency:function () {
            $('button[type=reset]').on('click',function(){
                $('input[name=start_date]').remove();
                $('input[name=end_date]').remove();

            });
            var form = $('form');
            var city_id = $('input[name=city_id]').val();
            var city_name = $('input[name=city_name]').val();
            var url = '/admin/company/rank/get_echart_data?city_id='+city_id+'&type='+1;

            // 基于准备好的dom，初始化echarts实例
            var myChart = Echarts.init(document.getElementById('echart'));

            // 指定图表的配置项和数据
            var option = {
                title: {
                    text: '代理业绩排行',
                    subtext: city_name
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['成交额','代理业绩']
                },
                xAxis: {
                    data: []
                },
                yAxis: {},
                series: [{
                    name: '成交额',
                    type: 'bar',
                    data: []
                },
                    {
                        name: '代理业绩',
                        type: 'bar',
                        data: []
                    },
                ]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);

            var data = JSON.parse($('input[name=data]').val());
            if(data.length === 0){
                $('#echart').hide();
            }else{
                option.xAxis.data = data.name;
                option.series[0]['data'] = data.price;
                option.series[1]['data'] = data.number;
                myChart.setOption(option);
            }


            // 基于准备好的dom，初始化echarts实例
            var myChart1 = Echarts.init(document.getElementById('echart_amount'), 'walden');
            // 指定图表的配置项和数据
            var option1 =  {
                title: {
                    text: '市级交易额与成交量',
                    subtext: city_name
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                legend: {
                    data: ['交易额', '成交量']
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                // toolbox: {
                //     show: false,
                //     feature: {
                //         magicType: {show: true, type: ['stack', 'tiled']},
                //         saveAsImage: {show: true}
                //     }
                // },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    // data: Orderdata.column
                    data: []
                },
                // yAxis: {
                //
                // },
                // grid: [{
                //     left: 'left',
                //     top: 'top',
                //     right: '35',
                //     bottom: 30
                // }],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                series: [{
                    name: '交易额',
                    type: 'line',
                    smooth: true,
                    areaStyle: {
                        normal: {
                        }
                    },
                    lineStyle: {
                        normal: {
                            width: 1.5
                        }
                    },
                    // data: Orderdata.paydata
                    data:[]
                },
                    {
                        name: '成交量',
                        type: 'line',
                        smooth: true,
                        areaStyle: {
                            normal: {
                            }
                        },
                        lineStyle: {
                            normal: {
                                width: 1.5
                            }
                        },
                        // data: Orderdata.createdata
                        data: []
                    }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart1.setOption(option1);
            $.get(url).done(function(res){
                if(res.code != 1){
                    return;
                }
                var data = JSON.parse(res.data);
                var order_data = data['order'];

                option1.xAxis.data = order_data.date_arr;
                option1.series[0]['data'] = order_data.amount_arr;
                option1.series[1]['data'] = order_data.count_arr;
                myChart1.setOption(option1);

            })






            form.attr('action',url);

            Form.api.bindevent(form,function(data,ret){
                // var data = res.data;
                var data = JSON.parse(data);
                var rank_data = JSON.parse(data['rank']['json_data']);

                var order_data = data['order'];

                option.xAxis.data = rank_data.name;
                option.series[0]['data'] = rank_data.price;
                option.series[1]['data'] = rank_data.number;
                myChart.setOption(option);

                option1.xAxis.data = order_data.date_arr;
                option1.series[0]['data'] = order_data.amount_arr;
                option1.series[1]['data'] = order_data.count_arr;
                myChart1.setOption(option1);
            });






            $(window).resize(function () {
                myChart.resize();
                myChart1.resize();
            });


        },

        // diagram: function(){
        //
        //
        // }
    };

    return Controller;
});