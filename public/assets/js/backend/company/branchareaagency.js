define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'company/branch_area_agency/index',
                    add_url: 'company/branch_area_agency/add',
                    edit_url: 'company/branch_area_agency/edit',
                    del_url: 'company/branch_area_agency/del',
                    multi_url: 'company/branch_area_agency/multi',
                    table: 'property',

                }
            });

            var table = $("#table");

            //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
               // 这里可以对params值进行修改,从而影响搜索条件
               //  var province_id,city_id,area_id;
               //  var filter = JSON.parse(params.filter);
               //  var op = JSON.parse(params.op);
               //  province_id = $('select[name="province_id"]').val();
               //  city_id = $('select[name="city_id"]').val();
               //  area_id = $('select[name="area_id"]').val();
               //  if(province_id){
               //      filter['province_id'] = province_id;
               //      op['province_id'] = '=';
               //  }
               //  if(city_id){
               //      filter['city_id'] = city_id;
               //      op['city_id'] = '=';
               //  }else{
               //      delete filter['city_id'];
               //  }
               //  if(!area_id){
               //      delete filter['area_id'];
               //  }
               //  params.filter = JSON.stringify(filter);
               //  params.op = JSON.stringify(op);

            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel'],
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'id', title: __('id'),operate: false},
                        {field: 'phone', title: __('手机号'),operate:'LIKE', placeholder: '手机号'},
                        {field: 'agency.realname', title: __('真实姓名'),operate:false, placeholder: '真实姓名'},
                        {field: 'agency.card', title: __('身份证号'),operate:false, placeholder: '身份证号码'},
                        {field: 'area', title: __('所属地区'),operate: false},
                        {field: 'time', title: __('添加时间'),operate: false,formatter: Table.api.formatter.datetime, operate: 'BETWEEN',type: 'datetime', addclass: 'datetimepicker',data: 'data-date-format="YYYY-MM-DD"'},
                        {field: 'open_share', title: __('是否开启分润'),operate: false,formatter: Controller.api.formatter.open_share},
                        {field: 'agency.is_ok', title: __('是否开通'),operate: false,formatter: Controller.api.formatter.is_ok},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Controller.api.formatter.edit}

                    ]
                ],
                queryParams: function (params) {
                    var city_id = $('input[name=city_id]').val();
                    params.data = '{"city_id":'+city_id+'}'
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        filter: params.filter,
                        op: params.op,
                        offset: params.offset,
                        limit: params.limit,
                        data:params.data
                    };
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                open_share: function(value, row, index){
                    if(value == 1){
                        var str1 = '是';
                    }else{
                        var str1 = '否';

                    }
                    return str1;
                    // return "<a href='javascript:;' class='btn btn-" + (value ? "info" : "default") + " btn-xs btn-change' data-id='"
                    //     + row.id + "' data-params='open_share=" + (value ? 0 : 1) + "'>" + (value ? __('Yes') : __('No')) + "</a>";
                },
                is_ok: function(value, row, index){
                    if(value == 1){
                        var str1 = '是';
                    }else{
                        var str1 = '否';

                    }
                    return str1;
                    // return "<a href='javascript:;' class='btn btn-" + (value==1 ? "info" : "default") + " btn-xs btn-change' data-id='"
                    //     + row.id + "' data-params='is_ok=" + (value==1? 2 : 1) + "'>" + (value == 1 ? __('Yes') : __('No')) + "</a>";
                },
                edit: function(value, row, index){
                    var that = $.extend({}, this);
                    var table = $(that.table).clone(true);
                    $(table).data("operate-del", null);
                    that.table = table;
                    return Table.api.formatter.operate.call(that, value, row, index);
                }

            },
        }
    };
    return Controller;
});