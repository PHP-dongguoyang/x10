define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'integral/activity/index',
                    add_url: 'integral/activity/add',
                    edit_url: 'integral/activity/edit',
                    del_url: 'integral/activity/del',
                    multi_url: 'integral/activity/multi',
                    table: 'integrallist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'integrallistID',
                sortName: 'integrallistID',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'integrallistID', title: __('Integrallistid')},
                        // {field: 'userID', title: __('Userid')},
                        {field: 'user_account', title: __('用户ID'),operate:false},
                        {field: 'integral', title: __('Integral'),operate:false},//积分
                        // {field: 'type', title: __('Type')},
                        {field: 'property', title: __('交易类型'),searchList: { '6':__('积分抽奖'),'5':__('积分兑换')}, style: 'min-width:100px;'},
                        // {field: 'id', title: __('Id')},
                        // {field: 'note', title: __('Note')},
                        {field: 'time', title: __('Time'), operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                            {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-info btn-addtabs', url: 'integral/activity/edit'}
                        ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons}
                    ]
                ],
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel']
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});