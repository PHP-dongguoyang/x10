define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'integral/integrallist/index',
                    add_url: 'integral/integrallist/add',
                    edit_url: 'integral/integrallist/edit',
                    del_url: 'integral/integrallist/del',
                    multi_url: 'integral/integrallist/multi',
                    table: 'integrallist',
                }
            });

            var table = $("#table");
                //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
                //这里可以对params值进行修改,从而影响搜索条件
                var province_id,city_id,area_id;
                var filter = JSON.parse(params.filter);
                var op = JSON.parse(params.op);
                province_id = $('select[name="province_id"]').val();
                city_id = $('select[name="city_id"]').val();
                area_id = $('select[name="area_id"]').val();
                if(province_id){
                    filter['province_id'] = province_id;
                    op['province_id'] = '=';
                }
                if(city_id){
                    filter['city_id'] = city_id;
                    op['city_id'] = '=';
                }else{
                    delete filter['city_id'];
                }
                if(!area_id){
                    delete filter['area_id'];
                }
                params.filter = JSON.stringify(filter);
                params.op = JSON.stringify(op);

                // console.log(params);
            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'integrallistID',
                sortName: 'integrallistID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'integrallistID', title: __('积分ID'),operate:false},
                        // {field: 'userID', title: __('用户ID')},
                        {field: 'order_num', title: __('订单号'),operate:false},
                        {field: 'order_type', title: __('交易类型'),operate:false},
                        {field: 'integral', title: __('积分数'),operate:false},
                        {field: 'city', title: __('地区'),operate:false},
                        // {field: 'id', title: __('Id')},
                        // {field: 'note', title: __('积分记录')},
                        {field: 'area_id', title: __('省市'), searchList: function () {
                            return Template('categorytpl', {});
                        },visible:false
                        },
                        {field: 'time', title: __('积分赠送时间'), operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                            {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-info btn-addtabs', url: 'integral/integrallist/edit'}
                        ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons}
                    ]
                ],
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel']
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});