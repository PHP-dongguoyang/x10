define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'integral/register/index',
                    add_url: 'integral/register/add',
                    edit_url: 'integral/register/edit',
                    del_url: 'integral/register/del',
                    multi_url: 'integral/register/multi',
                    table: 'integrallist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'integrallistID',
                sortName: 'integrallistID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'integrallistID', title: __('Integrallistid'),operate:false},
                        // {field: 'userID', title: __('Userid')},
                        {field: 'user_account', title: __('会员ID'),operate:false},
                        {field: 'integral', title: __('Integral'),operate:false},
                        // {field: 'type', title: __('Type')},
                        {field: 'property', title: __('积分类型'),operate:false},
                        // {field: 'id', title: __('Id')},
                        // {field: 'note', title: __('Note')},
                        {field: 'time', title: __('Time') , operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD"',formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                            {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-info btn-addtabs', url: 'integral/register/edit'}
                        ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons}
                    ]
                ],
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel']
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});