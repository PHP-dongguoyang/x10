define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/tuanorder/index',
                    add_url: 'tuan/tuanorder/add',
                    edit_url: 'tuan/tuanorder/edit',
                    del_url: 'tuan/tuanorder/del',
                    multi_url: 'tuan/tuanorder/multi',
                    table: 'tuan_order'
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'consumeID',
                sortName: 'consumeID',
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'consumeID', title: __('编号'),operate: false},
                        {field: 'ordernum', title: __('Order_num')},
                        {field: 'user.account', title: __('User.account')},
                        {field: 'fees', title: __('Price'),operate: false},
                        {field: 'pay_type', title: __('Pay_type'),operate: false},
                        {field: 'time', title: __('Pay_time'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'},
                        {field: 'id', title: __('操作'), table: table, buttons: [
                            {name: 'detail', text: '查看详情', title: '充值详情', icon: 'fa fa-flash', classname: 'btn btn-xs btn-info btn-addtabs', url: 'tuan/tuanorder/edit'}
                        ], operate:false, formatter: Table.api.formatter.buttons}
                        //{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams : function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        filter: JSON.stringify({type: 1,status:2}),
                        op: JSON.stringify({type: '=',status:'='}),
                        offset: params.offset,
                        limit: params.limit,
                    };
                },
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '充值列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col == 3){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                }
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});