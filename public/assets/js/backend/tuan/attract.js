define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/attract/index',
                    add_url: 'tuan/attract/add',
                    edit_url: 'tuan/attract/edit',
                    del_url: 'tuan/attract/del',
                    multi_url: 'tuan/attract/multi',
                    table: 'attract',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'attractID',
                sortName: 'attractID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'attractID', title: __('序号'),operate:false},
                        {field: 'type', title: __('收益类型'),operate:false},
                        {field: 'user.account', title: __('收益会员ID'),operate:false},
                        {field: 'user.type', title: __('身份'),operate:false},
                        {field: 'fees', title: __('分润金额（元）'),formatter: Table.api.formatter.get_money,operate:false},
                        {field: 'time', title: __('到账时间'), formatter: Table.api.formatter.datetime,operate:false},
                    ]
                ],
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});