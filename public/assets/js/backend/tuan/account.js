define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/account/index',
                    add_url: 'tuan/account/add',
                    edit_url: 'tuan/account/edit',
                    del_url: 'tuan/account/del',
                    multi_url: 'tuan/account/multi',
                    table: 'consume',
                }
            });

            var table = $("#table");
            table.on('post-body.bs.table', function (e, data) {
                if(data.length != 0){
                    $('#account-item').html('');
                    var account_html = [];
                    var class_name ='';
                    $.each(data,function(i,item){
                        if(i%2 == 0){
                            class_name = 'list-righter';
                        }else{
                            class_name = '';
                        }
                        var str = '\
                        <div class="col-xs-6 col-md-6">\
                            <a href="'+item.url+'">\
                                <div class="col-xs-6 col-md-7 '+class_name+'">\
                                    <div class="'+item.class+'">\
                                        <div class="panel-body">\
                                            <div class="panel-title">\
                                                <span class="label label-success pull-right">'+item.words+'</span>\
                                                <h3>'+item.name+'</h3>\
                                            </div>\
                                            <div class="panel-content">\
                                                <h1 class="no-margins">'+item.value+'</h1>\
                                                <div class="stat-percent font-bold text-gray">\
                                                </div>';
                        if(i == 0){
/*                            str +=                  '<small>'+item.title+'</small>' +
                                                    '&nbsp;&nbsp;（<small>入账：'+item.in+'</small>' +
                                                    '&nbsp;&nbsp;<small>出账：'+item.out+'</small>）';*/
                            str +=                  '<small>'+item.title+'</small>';
                        }else{
                            str +=                  '<small>'+item.title+'</small>';
                        }
                        str +=              '</div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </a>\
                        </div>';
                        account_html.push(str);
                    });
                    $('#account-item').html(account_html);
                }
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'date',
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                showExport: false,
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                pagination:false,
                exportTypes: [ 'csv','excel'],
                columns: [
                    [
                        {field: 'date', visible:false,title: __('Time'),formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'}
                    ]
                ],
                queryParams : function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        // filter: JSON.stringify({type: '1,2,3,7'}),
                        // op: JSON.stringify({type: 'IN'}),
                        offset: params.offset,
                        limit: params.limit
                    };
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                url: function (value, row, index) {

                    //这里手动构造URL
                    url = value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="统计详情">' + '查看详情' + '</a>';

                }
            }
        }
    };
    return Controller;
});