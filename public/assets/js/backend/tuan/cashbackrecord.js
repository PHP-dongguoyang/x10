define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/cashbackrecord/index',
                    add_url: 'tuan/cashbackrecord/add',
                    edit_url: 'tuan/cashbackrecord/edit',
                    del_url: 'tuan/cashbackrecord/del',
                    multi_url: 'tuan/cashbackrecord/multi',
                    table: 'cashbackrecord',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'order_id',
                sortName: 'order_id',
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '店铺返现列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col <=6 && col >= 3){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                },
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'shop_id', title: __('shop_id'),events: Controller.api.events.shop_id, formatter: Controller.api.formatter.shop_id,visible:false},
                        {field: 'order_num', title: __('Order_num'),operate: false},
                        {field: 'title', title: __('Tuaninfo.title'),operate: false},
                        {field: 'order_type', title: __('Order_type'),operate: false},
                        {field: 'total_price', title: __('Total_price'),operate: false},
                        //{field: 'user_coupon', title: __('User_coupon'),operate: false},
                        //{field: 'integral', title: __('Integral'),operate: false},
                        {field: 'price', title: __('Price'),operate: false},
                        {field: 'back_money', title: __('Back_money'),operate: false},
                        {field: 'jm_price', title: __('减免金额'),operate: false},
                        {field: 'pay_type', title: __('Pay_type'),operate: false},
                        {field: 'back_status', title: __('Back_status'),operate: false},
                        {field: 'create_time', title: __('Create_time'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'},
                        {field: 'id', title: __('操作'), table: table, buttons: [
                                {name: 'detail', text: '详情', title: '详情标题', icon: 'fa fa-flash', classname: 'btn btn-xs btn-info btn-addtabs', url: 'tuan/cashbackrecord/edit'}
                            ], operate:false, formatter: Table.api.formatter.buttons}
                    ]
                ],
                queryParams: function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        filter: JSON.stringify({"pay_status":2}),
                        op: JSON.stringify({"pay_status":"="}),
                        offset: params.offset,
                        limit: params.limit,
                    };
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                shop_id: function (value, row, index) {
                    console.log(1);
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },

            },
            events: {//绑定事件的方法
                shop_id: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-ip': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='shop_id']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                        Toastr.info("执行了自定义搜索操作");
                    }
                },
                browser: {
                    'click .btn-browser': function (e, value, row, index) {
                        e.stopPropagation();
                        Layer.alert("该行数据为: <code>" + JSON.stringify(row) + "</code>");
                    }
                },
            },

        }

    };
    return Controller;
});