define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/accountcheck/index',
                    /*add_url: 'tuan/accountcheck/add',
                    edit_url: 'tuan/accountcheck/edit',
                    del_url: 'tuan/accountcheck/del',
                    multi_url: 'tuan/accountcheck/multi',*/
                    table: 'account_check',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate: false},
                        {field: 'saleroom', title: __('Saleroom')+' /(元)',operate: false},
                        {field: 'saleroom_third', title: __('Saleroom_third')+' /(元)',operate: false},
                        {field: 'saleroom_tuanli', title: __('Saleroom_tuanli')+' /(元)',operate: false},
                        {field: 'saleroom_cash', title: __('Saleroom_cash')+' /(元)',operate: false},
                        {field: 'saleroom_b2c', title: __('Saleroom_b2c')+' /(元)',operate: false},
                        {field: 'refund_amount_third', title: __('Refund_amount_third')+' /(元)',operate: false},
                        {field: 'refund_amount_tuanli', title: __('Refund_amount_tuanli')+' /(元)',operate: false},
                        {field: 'deposit', title: __('Deposit')+' /(元)',operate: false},
                        {field: 'deposit_finish', title: __('Deposit_finish')+' /(元)',operate: false},
                        {field: 'deposit_pending', title: __('Deposit_pending')+' /(元)',operate: false},
                        {field: 'deposit_b2c', title: __('Deposit_b2c')+' /(元)',operate: false},
                        {field: 'retention_value_shop', title: __('Retention_value_shop')+' /(元)',operate: false},
                        {field: 'retention_value_user', title: __('Retention_value_user')+' /(元)',operate: false},
                        {field: 'retention_value_branch', title: __('Retention_value_branch')+' /(元)',operate: false},
                        {field: 'system_recharge', title: __('System_recharge')+' /(元)',operate: false},
                        {field: 'third_recharge', title: __('Third_recharge')+' /(元)',operate: false},
                        {field: 'shop_profit', title: __('Shop_profit')+' /(元)',operate: false},
                        {field: 'shop_profit_back', title: __('Shop_profit_back')+' /(元)',operate: false},
                        {field: 'selearnings', title: __('Selearnings')+' /(元)',operate: false},
                        {field: 'agent_attract', title: __('Agent_attract')+' /(元)',operate: false},
                        {field: 'attract', title: __('Attract')+' /(元)',operate: false},
                        {field: 'shop_attract', title: __('Shop_attract')+' /(元)',operate: false},
                        {field: 'user_attract', title: __('User_attract')+' /(元)',operate: false},
                        {field: 'branch_attract', title: __('Branch_attract')+' /(元)',operate: false},
                        {field: 'cash_pool_in', title: __('Cash_pool_in')+' /(元)',operate: false},
                        {field: 'cash_pool_out', title: __('Cash_pool_out')+' /(元)',operate: false},
                        {field: 'subtract', title: __('Subtract')+' /(元)',operate: false},
                        {field: 'tuanli_subsidy', title: __('Tuanli_subsidy')+' /(元)',operate: false},
                        {field: 'tuanli_value', title: __('Tuanli_value')+' /(元)',operate: false},
                        {field: 'account_date', title: __('Account_date'),formatter: Table.api.formatter.date,operate: 'BETWEEN', type: 'date', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD"'},
                        {field: 'account_time', title: __('Account_time'), formatter: Table.api.formatter.datetime,operate: false},
                        {field: 'operate', title: __('Operate'), table: table,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/accountcheck/edit'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                search: false,//快速搜索
                showToggle: false,
                showColumns: false,
                commonSearch: true, //是否启用通用搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                exportTypes: [ 'csv','excel']
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});