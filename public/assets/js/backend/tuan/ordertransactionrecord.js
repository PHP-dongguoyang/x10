


define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form ,Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/ordertransactionrecord/index',
                    add_url: 'tuan/ordertransactionrecord/add',
                    edit_url: 'tuan/ordertransactionrecord/edit',
                    del_url: 'tuan/ordertransactionrecord/del',
                    multi_url: 'tuan/ordertransactionrecord/multi',
                    table: 'ordertransactionrecord'
                }
            });
            //在普通搜索提交搜索前
            var table = $("#table");
            //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
                //这里可以对params值进行修改,从而影响搜索条件
                var province_id,city_id,area_id;
                var filter = JSON.parse(params.filter);
                var op = JSON.parse(params.op);
                province_id = $('select[name="shop.province_id"]').val();
                city_id = $('select[name="shop.city_id"]').val();
                area_id = $('select[name="shop.area_id"]').val();
                if(province_id){
                    filter['shop.province_id'] = province_id;
                    op['shop.province_id'] = '=';
                }else{
                    delete filter['shop.province_id'];
                    delete op['shop.province_id'];
                }
                if(city_id){
                    filter['shop.city_id'] = city_id;
                    op['shop.city_id'] = '=';
                }else{
                    delete filter['shop.city_id'];
                    delete op['shop.city_id'];
                }
                if(area_id){
                    filter['shop.area_id'] = area_id;
                    op['shop.area_id'] = '=';
                }else{
                    delete filter['shop.area_id'];
                    delete op['shop.area_id'];
                }
                params.filter = JSON.stringify(filter);
                params.op = JSON.stringify(op);

                // console.log(params);
            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });

            table.on('load-success.bs.table', function (e, data) {
                var content="交易总金额："+data.extend.transaction_price+"&nbsp;&nbsp;实付金额："+data.extend.price;
                $("#toolbar > .btn-group > a").html(content);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'order_id',
                sortName: 'order_id',
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '交易列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col == 7 || col == 8 || col == 9){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                },
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'user_id', title: __('userID'),visible:false},
                        {field: 'order_id', title: __('订单ID'),operate: false},
                        {field: 'account', title: __('商家ID'),operate: false},
                        {field: 'order_num', title: __('Order_num')},
                        {field: 'order_type', title: __('Order_type'),operate: false},
                        {field: 'shop.shop_name', title: __('Shop.shop_name'), operate: 'LIKE %...%', placeholder: '关键字，模糊搜索'},
                        {field: 'phone', title: __('绑定手机号'),operate: false},
                        {field: 'industry', title: __('Industry'),operate: false},
                        {field: 'price_subtract', title: __('Price'),operate: false},
                        {field: 'price', title: '实付金额 (元)',operate: false},
                        {field: 'money4', title:'服务税额（元）',operate: false},
                        // {field: 'money3', title:'实际分润（元）',operate: false},
                        {field: 'pay_type', title: __('Pay_type'),searchList: {1:'余额支付',2:'支付宝',3:'微信',4:'现金支付'}},
                        {field: 'pay_status', title: __('支付状态'),searchList:{2:'已支付','-1':'退款中','-2':'已退款'}},
                        {field: 'order_status', title: __('订单状态'),searchList:{1:'待使用',2:'待评价',3:'已评价'}},
                        {   field: 'pay_time',
                            title: __('支付时间'),
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'
                        },
                        {field: 'city', title: __('地区'),operate: false},
                        {field: 'pca', title: __('省市区'),visible:false, searchList: function () {
                                return Template('categorytpl', {});
                            },
                        },
                        {field: 'id', title: __('操作'), table: table, buttons: [
                                {name: 'detail', text: '详情', title: '详情标题', icon: 'fa fa-flash', classname: 'btn btn-xs btn-info btn-addtabs', url: 'tuan/Ordertransactionrecord/edit'}
                            ], operate:false, formatter: Table.api.formatter.buttons}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                user_id: function (value, row, index) {
                    console.log(1);
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },
            },
            events: {//绑定事件的方法
                user_id: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-ip': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='user_id']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                    }
                }
            }
        },
    };
    return Controller;
});

function fenyun(order_id,url) {
    // alert(url+"/index"+ "?"+ 'order_id' + "=" + order_id)
    window.location.href=(url+"/index"+ "?"+ 's.id' + "=" + order_id)
}