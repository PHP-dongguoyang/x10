define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init();

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'tuan/earningsdetails/index',
                pk: 'attractID',
                sortName: 'attractID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'source', title: __('分润ID'),events: Controller.api.events.source, formatter: Controller.api.formatter.source, visible:false},
                        {field: 'number', title: __('序号'),operate:false},
                        {field: 'type', title: __('收益类型'),searchList: {1:'会员管理收益',2:'商家管理收益',3:'创客区域收益',4:'创客管理收益',5:'商家收益'}},
                        {field: 'user.account', title: __('收益会员ID'),operate:false},
                        {field: 'user.type', title: __('身份'),operate:false},
                        {field: 'fees', title: __('分润金额（元）'),formatter: Table.api.formatter.get_money,operate:false},
                        {field: 'time', title: __('到账时间'), formatter: Table.api.formatter.datetime,operate:false}
                    ]
                ],
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                source: function (value, row, index) {
                    console.log(1);
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },
            },
            events: {//绑定事件的方法
                source: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-ip': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='source']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                        Toastr.info("执行了自定义搜索操作");
                    }
                },
                browser: {
                    'click .btn-browser': function (e, value, row, index) {
                        e.stopPropagation();
                        Layer.alert("该行数据为: <code>" + JSON.stringify(row) + "</code>");
                    }
                },
            },
        }
    };
    return Controller;
});