define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    table1: 'tuan/depositlist/table1',
                    table2: 'tuan/depositlist/table2'
                }
            });

            //绑定事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                //移除绑定的事件
                $(this).unbind('shown.bs.tab');
            });

            //必须默认触发shown.bs.tab事件
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");

            $('.drop_down').click(function(){
                $(this).next('ul').show();
            });



        },
        table:{
            first: function () {
                var table1 = $("#table1");

                //当表格数据加载完成时
                /*table1.on('load-success.bs.table', function (e, data) {
                    var content="打款总额："+data.extend.out_total+"&nbsp;&nbsp;手续费："+data.extend.rake_price;
                    $("#toolbar1 > .btn-group > a").html(content);
                });*/

                // 初始化表格
                table1.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar1',
                    pk: 'depositlistID',
                    sortName: 'depositlistID',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '全部',  //表格工作区名称
                        // tableName: '',
                        excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col != 7){
                                return !isNaN($(cell).text())?'\\@':'';
                            }else{
                                return '0\.00';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            {field: 'bank_no_num', title: __('单号数量'),operate:'>=',placeholder:'大于等于'},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            {field: 'user.account', title: __('账户ID')},
                            {field: 'user.phone', title: __('手机号'),visible: false},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'time', title: __('申请时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'remit_time', title: __('打款时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'audit_time', title: __('到账时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'finance_status', title: __('处理状态'),formatter: Controller.api.formatter.finance_status,searchList: {3: __('待打款'),4: __('打款失败'),5:__('打款成功'),7:__('到账成功'),6:__('银行驳回')}},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('Operate'), table: table1,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"depositlist.finance_status":3,"depositlist.status":"1,3"}),
                            op: JSON.stringify({"depositlist.finance_status":">=","depositlist.status":"in"}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table1);
            },
            second: function () {
                var table2 = $("#table2");

                // 初始化表格
                table2.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar2',
                    pk: 'depositlistID',
                    sortName: 'operation_time',
                    sortOrder:'asc',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '待打款',  //表格工作区名称
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col == 7){
                                return '0\.00';
                            }else{
                                return !isNaN($(cell).text())?'\\@':'';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            {field: 'user.account', title: __('账户ID')},
                            {field: 'user.phone', title: __('手机号'),visible: false},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            {field: 'bank', title: __('银行/支行信息'),operate:false},
                            {field: 'location', title: __('地区'),operate:false},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'operation_time', title: __('时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operate', title: __('操作'), table: table2,buttons: [
                                {name: 'edit', text: '填写', title: '填写', icon: 'fa fa-edit', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/edit'},
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ],operate:false , formatter: Table.api.formatter.buttons}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":3,"depositlist.status":1}),
                            op: JSON.stringify({"finance_status":"=","depositlist.status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格2绑定事件
                Table.api.bindevent(table2);
            },
            three: function () {
                var table3 = $("#table3");

                // 初始化表格
                table3.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar3',
                    pk: 'depositlistID',
                    sortName: 'remit_time',
                    sortOrder:'desc',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '打款失败',  //表格工作区名称
                        onMsoNumberFormat: function (cell, row, col) {
                            if( col == 2){
                                return '0\.00';
                            }else{
                                return !isNaN($(cell).text())?'\\@':'';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            // {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            // {field: 'bank_no_num', title: __('单号数量'),operate:'>=',placeholder:'大于等于'},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            {field: 'bank', title: __('银行/支行信息'),operate:false},
                            {field: 'location', title: __('地区'),operate:false},
                            {field: 'remit_time', title: __('打款时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'finance_note', title: __('原因'),operate:false},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('Operate'), table: table3,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":4,"depositlist.status":1}),
                            op: JSON.stringify({"finance_status":"=","depositlist.status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table3);
            },
            four: function () {
                var table4 = $("#table4");

                // 初始化表格
                table4.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar4',
                    pk: 'depositlistID',
                    sortName: 'remit_time',
                    sortOrder:'asc',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [0,11,12],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '打款成功',  //表格工作区名称
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col == 5){
                                return '0\.00';
                            }else{
                                return !isNaN($(cell).text())?'\\@':'';
                            }
                        }
                    },
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            {field: 'bank_no_num', title: __('单号数量'),operate:'>=',placeholder:'大于等于'},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            {field: 'user.account', title: __('账户ID'),visible: false},
                            {field: 'user.phone', title: __('手机号'),visible: false},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            // {field: 'bank', title: __('银行/支行信息'),operate:false},
                            // {field: 'location', title: __('地区'),operate:false},
                            {field: 'remit_time', title: __('打款时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('操作'), table: table4,buttons: [
                                {name: 'edit', text: '填写', title: '填写', icon: 'fa fa-edit', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/edit?type=1'},
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ],operate:false , formatter: Table.api.formatter.buttons}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":5,"depositlist.status":1}),
                            op: JSON.stringify({"finance_status":"=","depositlist.status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table4);
            },
            five: function () {
                var table5 = $("#table5");

                // 初始化表格
                table5.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar5',
                    pk: 'depositlistID',
                    sortName: 'audit_time',
                    sortOrder:'desc',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '到账成功',  //表格工作区名称
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col == 3 || col == 8){
                                return '0\.00';
                            }else{
                                return !isNaN($(cell).text())?'\\@':'';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            {field: 'bank_no_num', title: __('单号数量'),operate:'>=',placeholder:'大于等于'},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            {field: 'user.account', title: __('账户ID')},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            {field: 'rake_price', title: __('手续费(元)'),operate:false},
                            {field: 'remit_time', title: __('打款时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'audit_time', title: __('到账时间'),formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('Operate'), table: table5,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":7,"depositlist.status":3}),
                            op: JSON.stringify({"finance_status":"=","depositlist.status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table5);
            },
            six: function () {
                var table6 = $("#table6");

                // 初始化表格
                table6.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table2,
                    toolbar: '#toolbar6',
                    pk: 'depositlistID',
                    sortName: 'reject_time',
                    sortOrder:'desc',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '银行驳回',  //表格工作区名称
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col == 4){
                                return '0\.00';
                            }else{
                                return !isNaN($(cell).text())?'\\@':'';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'account_type', title: __('账户身份'),formatter: Controller.api.formatter.accountType,searchList: {1: __('商家'),2: __('非商家')}},
                            {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            {field: 'bank_no_num', title: __('单号数量'),operate:'>=',placeholder:'大于等于'},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            // {field: 'bank', title: __('银行/支行信息'),operate:false},
                            // {field: 'location', title: __('地区'),operate:false},
                            {field: 'reject_time', title: __('驳回时间'), formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'finance_note', title: __('原因'),operate:false},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('Operate'), table: table6,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":6,"depositlist.status":1}),
                            op: JSON.stringify({"finance_status":"=","depositlist.status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table6);
            },
            seven: function () {
                var table7 = $("#table7");

                //当表格数据加载完成时
                 table7.on('load-success.bs.table', function (e, data) {
                 var content="打款总额："+data.extend.out_total+"&nbsp;&nbsp;手续费："+data.extend.rake_price;
                 $("#toolbar7 > .btn-group > a").html(content);
                 });

                // 初始化表格
                table7.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.table1,
                    toolbar: '#toolbar7',
                    pk: 'depositlistID',
                    sortName: 'depositlistID',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    exportOptions:{
                        ignoreColumn: [],  //忽略某一列的索引
                        fileName: '提现列表',//文件名称设置
                        worksheetName: '全部',  //表格工作区名称
                        // tableName: '',
                        excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
                        onMsoNumberFormat: function (cell, row, col) {
                            if(col != 5 && col != 6){
                                return !isNaN($(cell).text())?'\\@':'';
                            }else{
                                return '0\.00';
                            }
                        }
                    },
                    columns: [
                        [
                            // {checkbox: true},
                            {field: 'depositlistID', title: __('序号'),operate:false},
                            {field: 'bank_tradeno', title: __('单号'),formatter: Controller.api.formatter.bank_tradeno,operate:'LIKE'},
                            {field: 'bank_no_num', title: __('单号数量'),operate:false,placeholder:'大于等于'},
                            {field: 'name', title: __('开户名'),operate:false},
                            {field: 'number', title: __('提现卡号'),operate:false},
                            {field: 'money', title: __('打款金额(元)'),operate:false},
                            {field: 'rake_price', title: __('手续费(元)'),operate:false},
                            {
                                field: 'time',
                                title: __('申请时间'),
                                formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {
                                field: 'remit_time',
                                title: __('打款时间'),
                                formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {
                                field: 'audit_time',
                                title: __('到账时间'),
                                formatter: Table.api.formatter.datetime,
                                operate: 'RANGE',
                                addclass: 'datetimerange'},
                            {field: 'finance_status', title: __('处理状态'),formatter: Controller.api.formatter.finance_status,searchList: {3: __('待打款'),4: __('打款失败'),5:__('打款成功'),7:__('到账成功'),6:__('银行驳回')}},
                            {field: 'type', title: __('提现类型'),formatter: Controller.api.formatter.type,searchList: {1: __('手动提现'),2: __('自动提现')}},
                            {field: 'operator', title: __('操作人'),operate:false},
                            {field: 'operate', title: __('Operate'), table: table7,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'tuan/depositlist/details'}
                            ], events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"finance_status":3,"status":"1,3"}),
                            op: JSON.stringify({"finance_status":">=","status":"in"}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });

                // 为表格1绑定事件
                Table.api.bindevent(table7);
            }
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                finance_status:function(value, row, index){
                    var arr = {3: '待打款', 4: '打款失败', 5: '打款成功', 7: '到账成功',6:'银行驳回'};
                    return arr[value];
                },
                type:function(value, row, index){
                    var arr = {1: '手动提现', 2: '自动提现'};
                    return arr[value];
                },
                accountType:function(value, row, index){
                    var arr = {1: '商家', 2: '非商家'};
                    return arr[value];
                },
                pay_account_id:function(value, row, index){
                    var arr = {0:'-',1: '景坤支付宝', 2: '景坤支付宝'};
                    return arr[value];
                },
                bank_tradeno:function(value, row, index){
                    var num = value.length;
                    if(num > 1){
                        var str = '<div id="dLabelBox" onmouseover="ul_show(this)" onmouseout="ul_hide(this)" class="drop_down" style="position: relative;">' +
                            '<span id="dLabel">' +
                            value[0] +
                            '</span>' +
                            '<ul style="width: 100%;min-width: 240px;position: absolute; top: 14px; left: 0; background-color: #fff; border: 1px solid #EEE; box-shadow: 2px 2px 4px #ccc; padding: 5px 10px; z-index: 999; display: none;">';

                        $.each(value,function(i){
                            if(i > 0){
                                str += '<li style="width: 100%; list-style: none; line-height: 24px;">'+value[i]+'</li>';
                            }
                        });

                        str += '</ul></div>';
                        return str;
                    }else{
                        return value[0];
                    }
                }
            }
        }
    };
    return Controller;
});
