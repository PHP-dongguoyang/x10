define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init();

            //绑定事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                //移除绑定的事件
                $(this).unbind('shown.bs.tab');
            });

            //必须默认触发shown.bs.tab事件
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        table: {
            first: function () {
                // 表格1
                var table1 = $("#table1");
                //在普通搜索提交搜索前
                table1.on('common-search.bs.table', function (event, table, params, query) {

                    //这里可以对params值进行修改,从而影响搜索条件
                    var filter = JSON.parse(params.filter);
                    if(typeof filter.price !== 'undefined'){
                        filter.price *= 100;
                        params.filter = JSON.stringify(filter);
                    }
                });

                table1.bootstrapTable({
                    url: 'tuan/accountstatistics/table1',
                    toolbar: '#toolbar1',
                    pk: 'order_id',
                    sortName: 'order_id',
                    showToggle: false,
                    showColumns: false,
                    commonSearch: true, //是否启用通用搜索
                    search: false, //是否启用快速搜索
                    searchFormVisible: true, //是否始终显示搜索表单
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'order_id', title: __('Order_id'),operate: false},
                            {field: 'order_num', title: __('Order_num'),operate:'LIKE'},
                            {field: 'order_type', title: __('Order_type'),searchList: {'1': __('团购'), '2': __('买单'),'3':__('充值')}, style: 'min-width:100px;'},
                            {field: 'shop.shop_name', title: __('Shop.shop_name'),operate: 'LIKE',},
                            {field: 'user.phone', title: __('消费会员手机号'),operate: 'LIKE',},
                            {field: 'price', title: __('Price')},
                            {field: 'pay_type', title: __('Pay_type'),searchList: {'2': __('支付宝'),'3':__('微信')}, style: 'min-width:100px;'},
                            // {field: 'create_time', title: __('Create_time'), operate: false,formatter: Table.api.formatter.datetime},
                            {field: 'pay_time', title: __('Pay_time'), operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',formatter: Table.api.formatter.datetime},
                            // {field: 'update_time', title: __('Update_time'), operate: false,formatter: Table.api.formatter.datetime},
                            {field: 'operate', title: __('Operate'), table: table1,buttons: [
                                {name: 'detail', text: '查看详情', title: '查看详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-info btn-dialog', url: 'tuan/accountstatistics/edit'}
                            ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons}
                        ]
                    ]
                });


                // 为表格1绑定事件
                Table.api.bindevent(table1);
            },
            second: function () {
                // 表格2
                var table2 = $("#table2");
                // 初始化表格
                table2.bootstrapTable({
                    url: 'tuan/accountstatistics/table2',
                    toolbar: '#toolbar2',
                    pk: 'consumeID',
                    sortName: 'consumeID',
                    showToggle: false,
                    showColumns: false,
                    searchFormVisible: true, //是否始终显示搜索表单
                    search: false, //是否启用快速搜索
                    exportDataType: "all", //all导出整个表,basic导出当前分页
                    exportTypes: [ 'csv','excel'],
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'consumeID', title: __('consumeID'),operate: false},
                            {field: 'ordernum', title: __('Ordernum'),operate:'LIKE'},
                            {field: 'user.account', title: __('Useraccount'),operate:false},
                            {field: 'user.type', title: __('Usertype'),operate: 'LIKE',searchList: {'1': __('创客'), '2': __('商家'),'3':__('VIP会员'),'4':__('普通会员')}, style: 'min-width:100px;'},
                            {field: 'type', title: __('出账类型'),operate: false},
                            {field: 'fees', title: __('出账金额'),operate: false},
                            {field: 'time', title: __('Time'), operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"',formatter: Table.api.formatter.datetime},
                            {
                                field: 'operate', title: __('Operate'), table: table2, buttons: [
                                {
                                    name: 'detail',
                                    text: '查看详情',
                                    title: '查看详情',
                                    icon: 'fa fa-list',
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    url: 'tuan/accountstatistics/billing_edit'
                                }
                            ], events: Table.api.events.buttons, formatter: Table.api.formatter.buttons
                            }
                        ]
                    ],
                    queryParams: function (params) {
                        return {
                            search: params.search,
                            sort: params.sort,
                            order: params.order,
                            filter: JSON.stringify({"type":"2,8","status":2}),
                            op: JSON.stringify({"type":"in","status":"="}),
                            offset: params.offset,
                            limit: params.limit
                        };
                    }
                });
                // 为表格2绑定事件
                Table.api.bindevent(table2);
            },
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                userID: function (value, row, index) {

                    //这里手动构造URL
                    url = "tuan/ordertransactionrecord/index?" + "user_id=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="btn btn-xs btn-info btn-addtabs" title="会员消费详情">' + '查看详情' + '</a>';

                },
                ip: function (value, row, index) {
                    //这里手动构造URL
                    url = "tuan/attract?" + 'source' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="label label-success addtabsit" title="' + '查看详情' + '">' + '查看详情' + '</a>';

                    //方式二,直接调用Table.api.formatter.addtabs
                    return Table.api.formatter.addtabs(value, row, index, url);
                },
                shop: function (value, row, index) {
                    //这里手动构造URL
                    url = "?" + 'source' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="label label-success addtabsit" title="' + '查看详情' + '">' + '查看详情' + '</a>';

                    //方式二,直接调用Table.api.formatter.addtabs
                    return Table.api.formatter.addtabs(value, row, index, url);
                }
            }
        }
    };
    return Controller;
});