define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'tuan/selearnings/index',
                    add_url: 'tuan/selearnings/add',
                    edit_url: 'tuan/selearnings/edit',
                    del_url: 'tuan/selearnings/del',
                    multi_url: 'tuan/selearnings/multi',
                    table: 'selearnings'
                }
            });

            var table = $("#table");
            //在普通搜索提交搜索前
            table.on('common-search.bs.table', function (event, table, params, query) {
                //这里可以对params值进行修改,从而影响搜索条件
                var province_id,city_id,area_id;
                var filter = JSON.parse(params.filter);
                var op = JSON.parse(params.op);
                province_id = $('select[name="sp.province_id"]').val();
                city_id = $('select[name="sp.city_id"]').val();
                area_id = $('select[name="sp.area_id"]').val();
                if(province_id){
                    filter['sp.province_id'] = province_id;
                    op['sp.province_id'] = '=';
                }else{
                    delete filter['sp.province_id'];
                    delete op['sp.province_id'];
                }
                if(city_id){
                    filter['sp.city_id'] = city_id;
                    op['sp.city_id'] = '=';
                }else{
                    delete filter['sp.city_id'];
                    delete op['sp.city_id'];
                }
                if(area_id){
                    filter['sp.area_id'] = area_id;
                    op['sp.area_id'] = '=';
                }else{
                    delete filter['sp.area_id'];
                    delete op['sp.area_id'];
                }
                params.filter = JSON.stringify(filter);
                params.op = JSON.stringify(op);

                // console.log(params);
            });

            //在普通搜索渲染后
            table.on('post-common-search.bs.table', function (event, table) {
                Form.events.cxselect($("form", table.$commonsearch));
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'selearningsID',
                sortName: 'selearningsID',
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'selearningsID', title: __('Selearningsid'),operate:false},
                        {field: 'order_num', title: __('订单号'),operate:false},
                        {field: 'origin', title: __('交易类型'),formatter:Table.api.formatter.get_deal_type,operate:false},
                        {field: 'fees', title: __('服务税额（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'source10', title: __('商家管理收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'source8', title: __('会员管理收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'source7', title: __('创客管理收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'source9', title: __('创客区域收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'source11', title: __('商家收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'system', title: __('平台收益（元）'),formatter:Table.api.formatter.get_money,operate:false},
                        {field: 'pay_type', title: __('支付方式'),formatter:Table.api.formatter.get_pay_type,operate:false},
                        {field: 'pca', title: __('地区'),operate:false},
                        {field: 'sp.area_id', title: __('省市'), searchList: function () {
                                return Template('categorytpl', {});
                            },visible:false
                        },
                        {field: 'o.order_num', title: __('订单号'),visible:false},
                        {field: 'time', title: __('交易时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'},
                        {field: 'finance_note', title: __('原因'),operate:false},
                        {field: 'selearningsID', title: '查看详情', formatter: Controller.api.formatter.source,operate:false},
                        {field: 's.id', title: __('orderID'),visible:false}
                    ]
                ],
                commonSearch: true, //是否启用通用搜索
                search: false, //是否启用快速搜索
                searchFormVisible: true, //是否始终显示搜索表单
                exportDataType: "all", //all导出整个表,basic导出当前分页
                showToggle: false,
                showColumns: false,
                exportTypes: [ 'csv','excel'],
                exportOptions:{
                    ignoreColumn: [],  //忽略某一列的索引
                    fileName: '分润列表',//文件名称设置
                    worksheetName: 'sheet1',  //表格工作区名称
                    onMsoNumberFormat: function (cell, row, col) {
                        if(col <=9 && col >= 3){
                            return '0\.00';
                        }else{
                            return !isNaN($(cell).text())?'\\@':'';
                        }
                    }
                },
                //指定搜索条件
                queryParams : function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        filter: JSON.stringify({is_profit_finish: 2}),
                        op: JSON.stringify({is_profit_finish: '='}),
                        offset: params.offset,
                        limit: params.limit
                    };
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },

        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                source: function (value, row, index) {
                    //这里手动构造URL
                    url = "tuan/earningsdetails/index?" + 'source' + "=" + value;

                    //方式一,直接返回class带有addtabsit的链接,这可以方便自定义显示内容
                    return '<a href="' + url + '" class="label label-success addtabsit" title="' + '查看分润详情' + '">' + '查看详情' + '</a>';
                }
            }
        }
    };
    return Controller;
});