<?php

namespace app\admin\model;

use think\Model;

class Consume extends Model
{
    // 表名
    protected $name = 'consume';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
//        'time_text'
    ];


    public function user()
    {
        return $this->hasOne('User', 'userID', 'userID','','LEFT')->setEagerlyType(0);
    }

    public function property()
    {
        return $this->belongsTo('Property', 'userID', 'userID','','LEFT')->setEagerlyType(0);
    }

    public function depositlist()
    {
        return $this->hasOne('Depositlist','consumeID','consumeID','','LEFT')->setEagerlyType(0);
    }
    public function autonym()
    {
        return $this->hasOne('Autonym','userID','userID')->setEagerlyType(0);
    }

    public function recharge()//只有查询充值的时候，才有用到此关联
    {
        return $this->hasOne('TuanOrder','order_id','id','','LEFT')->setEagerlyType(0);
    }

    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }


}
