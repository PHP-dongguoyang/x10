<?php

    namespace app\admin\model;

    use think\Model;

    class Shop extends Model
    {
        // 表名
        protected $name = 'shop';
        public function user()
        {
            return $this->hasOne('User', 'userID', 'user_id')->setEagerlyType(0);
        }
    }
