<?php

namespace app\admin\model;

use think\Model;

class TuanOrder extends Model
{
    // 表名
    protected $name = 'tuan_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        'create_time_text',
        'pay_time_text',
        'update_time_text'
    ];






    public function getCreateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['create_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['pay_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getUpdateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['update_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setCreateTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setPayTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setUpdateTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'userID','','LEFT')->setEagerlyType(0);
    }
    public function shop()
    {
        return $this->belongsTo('Shop', 'shop_id', 'shop_id','','LEFT')->setEagerlyType(0);
    }

    public function selearnings()
    {
        return $this->hasMany('Selearnings', 'id', 'order_id');
    }
    public function tuaninfo()
    {
        return $this->belongsTo('TuanInfo', 'tuan_id','tuan_id','','LEFT')->setEagerlyType(0);
    }
    public function tuan()
    {
        return $this->belongsTo('Tuan','tuan_id','tuan_id','','LEFT')->setEagerlyType(0);
    }

    public function tuanCode()
    {
        return $this->hasMany('TuanCode','order_id','order_id');
    }
}
