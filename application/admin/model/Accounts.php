<?php
/**
 * Created by PhpStorm.
 * User: win7
 * Date: 2018/4/9
 * Time: 15:17
 */

namespace app\admin\model;


use think\Model;

class Accounts extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $table = 'dy_accounts';
}