<?php

namespace app\admin\model;

use think\Model;

class Depositlist extends Model
{
    // 表名
    protected $name = 'depositlist';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
//    protected $append = [
//        'time_text',
//        'audit_time_text',
//        'pay_time_text'
//    ];






//    public function getTimeTextAttr($value, $data)
//    {
//        $value = $value ? $value : $data['time'];
//        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
//    }
//
//
//    public function getAuditTimeTextAttr($value, $data)
//    {
//        $value = $value ? $value : $data['audit_time'];
//        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
//    }
//
//
//    public function getPayTimeTextAttr($value, $data)
//    {
//        $value = $value ? $value : $data['pay_time'];
//        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
//    }
//
//    protected function setTimeAttr($value)
//    {
//        return $value && !is_numeric($value) ? strtotime($value) : $value;
//    }
//
//    protected function setAuditTimeAttr($value)
//    {
//        return $value && !is_numeric($value) ? strtotime($value) : $value;
//    }
//
//    protected function setPayTimeAttr($value)
//    {
//        return $value && !is_numeric($value) ? strtotime($value) : $value;
//    }

    public function consume()
    {
        return $this->hasOne('User', 'consumeID', 'consumeID')->setEagerlyType(0);
    }

    public function price(){
        return $this->hasOne('consume', 'consumeID', 'consumeID')->setEagerlyType(0);
    }

    public function card(){
        return $this->hasOne('card','cardID','cardID')->setEagerlyType(0);
    }

    public function autonym(){
        return $this->hasOne('autonym','userID','userID')->setEagerlyType(0);
    }

    public function consume1(){
        return $this->hasOne('consume', 'consumeID', 'consumeID')->setEagerlyType(0);
    }

    public function user(){
        return $this->hasOne('user','userID','userID')->setEagerlyType(0);
    }
}
