<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class Selearnings extends Model
{
    // 表名
    protected $name = 'selearnings';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'time_text'
    ];
    

    



    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function Tuanorder()
    {
        return $this->belongsTo('TuanOrder', 'id', 'order_id')->setEagerlyType(0);
    }

    /**
     * 获取商家所在的省-市-区
     * @author: chenhan
     * @date:  2017年12月7日
     * @param array   $arr    省市区id数组
     * @return string
     */
    public function getPCA($arr){
        if(empty($arr))
            return '';

        $pac = Db::name('city')
            ->where(['cityID'=>['in',$arr]])
            ->column('name');
        if($pac){
            return join('-',$pac);
        }else{
            return '';
        }
    }
}
