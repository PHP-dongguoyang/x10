<?php

namespace app\admin\model;

use think\Model;

class Attract extends Model
{
    // 表名
    protected $name = 'attract';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        'time_text'
    ];

    public function getTypeAttr($value)
    {
        $type = [1=>'会员管理收益',2=>'商家管理收益',3=>'创客区域收益',4=>'创客管理收益',5=>'商家收益'];
        return $type[$value];
    }

    public function setTypeAttr($value)
    {
        $type = ['会员管理收益'=>1,'商家管理收益'=>2,'创客区域收益'=>3,'创客管理收益'=>4,'商家收益'=>5];
        return $type[$value];
    }

    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function Selearnings()
    {
        return $this->belongsTo('Selearnings', 'source', 'selearningsID')->setEagerlyType(0);
    }

    public function User()
    {
        return $this->hasone('User', 'userID', 'userID')->setEagerlyType(0);
    }
}
