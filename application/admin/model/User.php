<?php

namespace app\admin\model;

use think\Model;

class User extends Model
{
    // 表名
    protected $name = 'user';

    public function getTypeAttr($value)
    {
        if(empty($value)){
            return $value;
        }
        $type = [1=>'创客',2=>'商家',3=>'VIP会员',4=>'普通会员'];
        return $type[$value];
    }

    public function setTypeAttr($value)
    {
        $type = ['创客'=>1,'商家'=>2,'VIP会员'=>3,4=>'普通会员'];
        return $type[$value];
    }
    
}
