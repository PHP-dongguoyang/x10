<?php

return [
    'ConsumeID'  =>  '编号',
    'Ordernum'  =>  '流水号',
    'Useraccount'  =>  '会员ID',
    'Usertype'  =>  '身份',
    'Fees'  =>  '提现金额（元）',
    'Propertyvalue'  =>  '交易后余额（元）',
    'Time'  =>  '交易时间',

];
