<?php

return [
    'Depositlistid'  =>  'id',
    'Consumeid'  =>  '钱包记录id',
    'Userid'  =>  '消费人id',
    'Cardid'  =>  '银行卡id',
    'Time'  =>  '添加时间',
    'Status'  =>  '资金处理审核状态1初始，2我方平台已通过，3第三方平台通过，4银行通过，-2我方平台审核失败，-3第三方平台审核失败，-4银行审核失败',
    'Id'  =>  '审核人id',
    'Audit_time'  =>  '审核时间',
    'Note'  =>  '审核备注',
    'Serial_number'  =>  '流水号',
    'Pay_status'  =>  '1未到账,2已到账',
    'Pay_time'  =>  '到账时间',
    'Pay_way'  =>  '提现渠道,2支付宝,3微信',
    'Mc_id'  =>  '提现商户号',
    'Transaction_number'  =>  '第三方(微信,支付宝)流水号',
    'Rake_price'  =>  '提现手续费',
    'Type'  =>  '提现类别1为手动提现,2为自动提现',
    'Bank_tradeno'  =>  '银行或支付宝打款单号',
    'Pay_account_id'  =>  '打款账户id',
    'Finance_status'  =>  '财务打款状态1待打款(对应status=1),2打款失败(对应status=2)3到账成功(对应status=3)4已打款(对应status=1)5银行驳回(对应status=2)'
];
