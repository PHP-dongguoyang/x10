<?php

return [
    'Order_id'  =>  '编号',
    'Price'  =>  '交易金额（元）',
    'Order_num'  =>  '订单号',
    'Order_type'  =>  '交易类型',
    'Shop.tel'  =>  '联系方式',
    'Shop.shop_name'  =>  '店铺名称',
    'Industry'  =>  '行业',
    'Money4'  =>  '分润总额（元）',
    'Money3'  =>  '实际分润（元）',
    'Pay_type'  =>  '支付方式',
    'Order_status'  =>  '订单状态',
    'Create_time'  =>  '创建时间',
    'City'  =>  '城市',
    'user_id' => '用户id',
];
