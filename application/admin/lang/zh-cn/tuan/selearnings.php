<?php

return [
    'Selearningsid'  =>  'id',
    'Muserid'  =>  '商家id',
    'Userid'  =>  '用户id',
    'Id'  =>  '来源id,与origin配合试用',
    'Origin'  =>  '来源,1为团购,2为优惠买单',
    'Fees'  =>  '费用',
    'Time'  =>  '时间',
    'Is_profit_finish'  =>  '是否已经计算分润，-1计算失败，0未计算，1计算中，2计算完成',
    'Profit_cal_count'  =>  '做分润计算的计算重试次数，10次以后不再计算',
    'Profit_cal_finish_at'  =>  '分润计算成功的完成时间，秒时间戳',
    'Remark'  =>  '计算失败或计算成功的备注'
];
