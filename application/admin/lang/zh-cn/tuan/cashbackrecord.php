<?php

return [
    'Order_id'  =>  '编号',
    'Price'  =>  '实付金额（元）',
    'Order_num'  =>  '订单号',
    'Tuaninfo.title'  =>  '团购名称',
    'Order_type'  =>  '交易类型',
    'Total_price'  =>  '订单金额（元）',
    'User_coupon'  =>  '优惠券抵扣（元）',
    'Integral'  =>  '积分抵扣（元）',
    'Back_money'  =>  '返现金额（元）',
    'Pay_type'  =>  '支付方式',
    'Back_status'  =>  '返现状态',
    'Create_time'  =>  '创建时间',
];
