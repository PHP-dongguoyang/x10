<?php

return [
    'Integrallistid'  =>  'id',
    'Userid'  =>  '用户id',
    'Integral'  =>  '积分',
    'Type'  =>  '类型',
    'Property'  =>  '积分属性',
    'Id'  =>  'id',
    'Note'  =>  '积分记录',
    'Time'  =>  '时间'
];
