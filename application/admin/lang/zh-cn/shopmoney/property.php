<?php

return [
    'Propertyid'  =>  'id',
    'Userid'  =>  '用户id',
    'Name'  =>  '资产名字',
    'Type'  =>  '资产类型',
    'Value'  =>  '资产',
    'Status'  =>  '正常',
    'Time'  =>  '更新时间'
];
