<?php

return [
    'Attractid'  =>  'id',
    'Userid'  =>  '代理商id',
    'Fees'  =>  '收益',
    'Id'  =>  '代理商关系id',
    'Type'  =>  '收益类型',
    'Source'  =>  '来源',
    'Status'  =>  '状态',
    'Time'  =>  '时间',
    'Is_profit_finish'  =>  ' 分润状态   -1分润失败,0待分润,1分润中,2分润完成'
];
