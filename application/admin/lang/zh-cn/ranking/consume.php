<?php

return [
    'Consumeid'  =>  'id',
    'Ordernum'  =>  '订单号',
    'Userid'  =>  '操作',
    'Fees'  =>  '消费金额/元',
    'Type'  =>  '消费类型',
    'Status'  =>  '状态码',
    'Time'  =>  '时间区间',
    'Source'  =>  '订单来源',
    'Id'  =>  '添加人id',
    'Note'  =>  '余额获取详情',
    'Remark'  =>  '转账备注',
    'Account' => '用户账号',
    'Phone' => '绑定手机号'
];
