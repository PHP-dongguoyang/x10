<?php

namespace app\admin\validate;

use think\Validate;

class Depositlist extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'bank_tradeno'  =>  'require',
        'pay_account_id'=>  'require',
        'finance_status'=>  'require'
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'bank_tradeno.require'  =>  '请填写流水单号',
        'pay_account_id.require'  =>  '请选择出款账户',
        'finance_status.require'  =>  '请选择打款结果'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => ['finance_status'],
        'audit'=> ['finance_status']
    ];
    
}
