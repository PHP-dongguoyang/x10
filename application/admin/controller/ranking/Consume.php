<?php

namespace app\admin\controller\ranking;

use app\common\controller\Backend;
use think\Db;
use app\admin\model\User;
use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Consume extends Backend
{
    
    /**
     * Consume模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 会员消费排行
     * User: fuhang
     * Email: 515934402@qq.com
     * Time: 2017/12/6 0006 下午 2:08
     * @return string|\think\response\Json
     */
    public function index()
    {
        return $this->view->fetch();
    }

    public function table1(){
        $this->model = model('Consume');
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //已付款
            $list1 = Db::name('consume')
                ->alias('c')
                ->join('user u','u.userID = c.userID')
                ->where($where)
                ->where(['c.type'=>['in',[3,7]],'c.status'=>2,])
                ->field('c.userID,sum(fees) as fees,phone,account')
                ->group('c.userID')
                ->select();

            //已退款
            $list2 = Db::name('consume')
                ->alias('c')
                ->join('user u','u.userID = c.userID')
                ->where($where)
                ->where(['c.type'=>8,'c.status'=>2,])
                ->field('c.userID,sum(fees) as fees')
                ->group('c.userID')
                ->select();

            $list2 = array_column($list2,'fees','userID');

            foreach ($list1 as $k1=>$v1){
                if(key_exists($v1['userID'],$list2)){
                    $v1['fees'] -= $list2[$v1['userID']];
                    if($v1['fees'] >= 0){
                        $list1[$k1] = $v1;
                    }else{
                        unset($list1[$k1]);
                    }
                }
            }

            if($list1){
                //总数
                $total = count($list1);

                //从高太低排序
                $arr = [];
                foreach($list1 as $k => $v){
                    $arr[] = $v['fees'];
                }
                array_multisort($arr,SORT_DESC ,$list1);

                //数组分页
                $list = array_slice((array)$list1,$offset,$limit);

                foreach ($list as $k=>$v){
                    //排名
                    $list[$k]['rank'] = $offset+$k+1;
                    //充值金额/元
                    $list[$k]['fees'] = price_format($v['fees']);
                }
            }else{
                //总数
                $total = 0;

                $list = [];
            }

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch('index');
    }

    public function table2(){
        $this->model = model('Shop');
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('pkey_name'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $map = [
                'source'    =>  12,
                'c.type'      =>  1,
                'c.status'    =>  2
            ];
            $total = Db::name('consume')
                ->alias('c')
                ->join('dy_user u','c.userID = u.userID','left')
                ->where($where)
                ->where($map)
                ->group('c.userID')
                ->count();

            $field = 'sum(fees) as fees,u.phone,u.account,time,c.userID';
            $list = Db::name('consume')
                ->alias('c')
                ->join('dy_user u','c.userID = u.userID','left')
                ->field($field)
                ->where($where)
                ->where($map)
                ->group('c.userID')
                ->order('fees desc')
                ->limit($offset,$limit)
                ->select();

            foreach ( $list as $k => $v){
                //显示排名
                $list[$k]['rank'] = $offset+$k+1;
                //查询店铺信息
                $shop = Db::name('shop')->where('user_id',$v['userID'])->find();
                $list[$k]['shop_id'] = $shop['shop_id'];
                $list[$k]['shop_name'] = $shop['shop_name'];
                //行业
                $list[$k]['cate'] = Db::name('industry')->where('industryID',$shop['cate_id'])->value('name');
                //拼接省市区
                $city = Db::name('city')
                    ->where('cityID','in',[$shop['province_id'],$shop['city_id'],$shop['area_id']])
                    ->column('name');
                $list[$k]['city'] = join('-',$city);
                //获取推荐人数
                $list[$k]['user_num'] = Db::name('user')
                    ->where([
                        'PID' => $v['userID'],
                        'type' => 3,
                        'attestation' => 7,
                    ]
                )->count();
                //处理金额
                $list[$k]['fees'] = price_format($v['fees']);
            }

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch('index');
    }

    public function table3(){
        $this->model = model('Attract');
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $total = Db::name('attract')
                ->alias('a')
                ->join('user u','u.userID = a.userID','left')
                ->where($where)
                ->field('attractID,a.userID,sum(fees) as fees,phone,account')
                ->group('a.userID')
                ->order('fees', 'desc')
                ->count();

            $list = Db::name('attract')
                ->alias('a')
                ->join('user u','u.userID = a.userID','left')
                ->where($where)
                ->field('attractID,a.userID,sum(fees) as fees,phone,account')
                ->group('a.userID')
                ->order('fees', 'desc')
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $k=>$v){
                $list[$k]['rank'] = $offset+$k+1;
            }

            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch('index');
    }
}
