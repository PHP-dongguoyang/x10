<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/25 0025
 * Time: 下午 4:32
 */

namespace app\admin\controller\company;


use app\admin\model\BranchOffice;
use app\admin\model\BranchOfficeInfo;
use app\admin\model\City;
use app\admin\model\Shop;
use app\admin\model\TuanOrder;
use app\common\controller\Backend;
use think\Controller;
use think\Db;

class Rank extends Backend
{

    protected $model = null;
    protected $relationSearch = false;

    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
        $this->model = model('BranchOffice');
    }

    /**
     * 市级排行
     * @author: yanggang
     * @date: 2018/7/26 0026
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function top()
    {
        $city_id = $this->request->get('city_id');
        $params = $this->request->param();
        $top_data = (new BranchOffice())->get_rank_top($city_id,$params);
        $data = $top_data['data'];
        $rank = $top_data['rank'];
        $json_data = $top_data['json_data'];

        $this->assign('data', $data);
        $this->assign('rank', $rank);
        $this->assign('city_id',$city_id);
        $this->assign('city_name', City::where('cityID',$city_id)->value('name'));

        $this->assign('json_data', $json_data);

        return $this->view->fetch();
    }

    /**
     * 区级排行
     * @author: yanggang
     * @date: 2018/7/26 0026
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function agency()
    {
        $city_id = $this->request->get('city_id');

        $params = $this->request->param();
        //区县名称
        $city_info = City::where(['cityID' => $city_id])->find();
        //城市名称
        $city['city_name'] = City::where(['cityID' => $city_info['PID']])->find();

        $data  = (new BranchOffice())->get_rank_agency($city_id,$params);
        $arr  = $data['arr'];
        $json_data  = $data['json_data'];
        $this->assign('data',$arr);
        $this->assign('city_id',$city_id);
        $this->assign('city_name', $city_info['name']);

        $this->assign('json_data', $json_data);

        return $this->view->fetch();
    }

    /**
     * 市级或区级的店铺交易额与交易量
     * @author: yanggang
     * @date: 2018/7/26 0026
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function get_echart_data()
    {
        if (!$this->request->has('city_id')){
            return json(['status'=>0,'msg'=>'参数丢失']);
        }
        $city_id = $this->request->get('city_id');
        $params = $this->request->param();

        if ($this->request->has('type')){ //区
            $map = ['area_id'=>$city_id];
            $rank_data = (new BranchOffice())->get_rank_agency($city_id,$params);
        }else{ //市
            $map = ['city_id'=>$city_id];
            $rank_data = (new BranchOffice())->get_rank_top($city_id,$params);
        }

        $shop_ids = Shop::where($map)->column('shop_id');

        $order_data = (new BranchOffice())->get_data($shop_ids,$params);
        $json = json_encode(["rank"=>$rank_data,"order"=>$order_data]);
        return ['code'=>1,'msg'=>'成功','data'=>$json];
    }

    public function distribution()
    {
//        $sql = "SELECT COUNT(1) as value,c.name from dy_shop s LEFT JOIN dy_city c ON c.cityID = s.city_id where closed = ? group BY city_id ORDER BY value DESC";
//        $res = Db::query($sql,[1]);

        $res = (new BranchOffice())
            ->alias('bo')
            ->field('name, c.lng, c.lat')
            ->join('city c','city_id = c.cityid')
            ->where(['bo.type'=>1,'bo.status'=>1,'c.type'=>3])
            ->select();


        $toolTipData = [];
        $geoCoordMap = [];
//        array_splice($res,10);
        foreach ($res as  $key=>$item){
            $geoCoordMap[$item['name']] = [$item['lng'], $item['lat']];
            $toolTipData[] = ['name'=>$item['name'],'value'=>[['name'=>$item['name'],'value'=>1],['name'=>'阿萨德','value'=>'1']]];
        }


        $toolTipData = json_encode($toolTipData);

        $geoCoordMap = json_encode($geoCoordMap);

        $this->assign('toolTipData',$toolTipData);
        $this->assign('geoCoordMap',$geoCoordMap);
        return $this->view->fetch();
    }

}