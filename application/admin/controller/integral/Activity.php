<?php

namespace app\admin\controller\integral;

use app\common\controller\Backend;

use think\Controller;
use think\Db;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Activity extends Backend
{
    
    /**
     * Integrallist模型对象
     */
    protected $model = null;
    protected $property;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Integrallist');
        $this->property = [
          3 => '积分抵扣',
          4 => '退款扣除积分',
          5 => '积分兑换',
          6 => '积分抽奖'
        ];
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('pkey_name'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->where('integrallist.property','in',[5,6])
                ->where('integrallist.type','=',2)
                ->with(['User'=>function($query){
                    $query->withField('account');
                }])
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where('property','in',[5,6])
                ->where('integrallist.type','=',2)
                ->with(['User'=>function($query){
                    $query->withField('account');
                }])
                ->group('userID,time')
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k => $v){
                $list[$k]['user_account'] = $v->User['account'];
//                $integral_get = $this->model->where(['time'=>$v->time , 'userID' => $v->userID , 'type' => 1])->value('integral');
//                $list[$k]['integral'] = $integral_get - $v['integral'];
                $list[$k]['property'] = $this->property[$v['property']];
                /*if ($v['type'] == 2){
                    $list[$k]['integral'] = '-'.$v['integral'];
                }*/
                $list[$k]['integral'] = '-'.$v['integral'];
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }

        $total_get = $this->model
            ->where(['type'=>1])
            ->where('property','in',[5,6])
            ->sum('integral');
        $total_use = $this->model
            ->where(['type'=>2])
            ->where('property','in',[5,6])
            ->sum('integral');
        $this->view->assign("total", $total_get-$total_use);//积分总数
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model
            ->where('integrallistID','=',$ids)
            ->with('User')
            ->find()->toArray();
        //查询出相应的兑换信息
        if ($row['property'] == 5){
            $mall = Db::name('mall_order')->where('order_id','=',$row['id'])->find();
            if ($mall['goods_type'] == 1){
                $goods = Db::name('goods')->where('goods_id','=',$mall['goods_id'])->find();
                $row['goods_name'] = $goods['name'];
                $row['address'] = $mall['address_id'];
            }elseif ($mall['goods_type'] == 2){
                $coupons = Db::name('coupon')->where('coupon_id','=',$mall['goods_id'])->find();
                $row['goods_name'] = $coupons['coupon_name'];
                $row['address'] = $mall['address_id'];
            }
        }elseif($row['property'] == 6){
            //查询出整个积分消耗过程
            if ($row['type'] == 1){
                $row['get'] = $row['integral'];
                $row['pay'] = $this->model->where(['time'=>$row['time'],'userID'=>$row['userID'],'type'=>2])->value('integral');
            }else{
                $row['get'] = $this->model->where(['time'=>$row['time'],'userID'=>$row['userID'],'type'=>1])->value('integral');
                $row['get'] = $row['get'] ? $row['get'] : 0;
                $row['pay'] = $row['integral'];
            }
        }
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
