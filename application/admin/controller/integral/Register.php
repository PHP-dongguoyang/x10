<?php

namespace app\admin\controller\integral;

use app\admin\model\User;
use app\common\controller\Backend;

use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Register extends Backend
{
    
    /**
     * Integrallist模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Integrallist');
    }

    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('pkey_name'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->where('property','in',[1,2])
                ->order($sort, $order)
                ->count();
            $user_model = new User();
            $list = $this->model
                ->where($where)
                ->where('property','in',[1,2])
                ->with(['User'=>function($query){
                    $query->withField('account');
                }])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k => $v){
                $list[$k]['user_account'] = $v->User['account'];
                if ($v['property'] == 1){
                    $list[$k]['property'] = '注册赠送';
                }elseif ($v['property'] == 2){
                $pid = $user_model->where('userID','=',$v['id'])->value('PID');
                    if ($v['userID'] == $pid){
                        $list[$k]['property'] = '推荐赠送';
                    }else{
                        $list[$k]['property'] = '间接推荐赠送';
                    }
                }
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        $total_get = $this->model
            ->where(['type'=>1])
            ->where('property','in',[1,2])
            ->sum('integral');
        $this->view->assign("total", $total_get);
        return $this->view->fetch();

    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model
            ->where('integrallistID','=',$ids)
            ->with(['User','Pid'])
            ->find()->toArray();
        $user_model = new User();
        if ($row['property'] == 2){
            if ($row['userID'] == $row['pid']['PID']){
                //直接推荐
                $row['type']            = 2;
                $row['register_user']       = $row['pid']['account'];
            }else{
                //间接推荐
                $row['type']            = 3;
                $row['register_user']       = $row['pid']['account'];
                $row['referee']             = $user_model->where('userID','=',$row['pid']['PID'])->value('account');
            }
        }else{
            $row['type']            = 1;
        }

        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
