<?php

namespace app\admin\controller\integral;

use app\admin\model\TuanOrder;
use app\common\controller\Backend;

use think\Controller;
use think\Request;
use think\db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Integrallist extends Backend
{
    protected $order_type;
    protected $pay_type;
    /**
     * Integrallist模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Integrallist');
        $this->order_type = [
            '1'=>'团购订单',
            '2'=>'优惠买单',
            '3'=>'充值',
        ];
        $this->pay_type = [
            '0'=>'未支付',
            '1'=>'余额支付',
            '2'=>'支付宝',
            '3'=>'微信支付',
        ];
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('pkey_name'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = Db::name('integrallist')
                ->alias('i')
                ->join('tuan_order t','t.order_id=i.id')
                ->join('shop s','s.shop_id=t.shop_id')
                ->where($where)
                ->where('i.property','=',3)
                ->where('i.type','=',1)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->count();

            $list = Db::name('integrallist')
                ->alias('i')
                ->join('tuan_order t','t.order_id=i.id')
                ->join('shop s','s.shop_id=t.shop_id')
                ->where($where)
                ->where('i.property','=',3)
                ->where('i.type','=',1)
                ->field('s.city_id as city_id,s.area_id as area_id,s.province_id as province_id,t.order_num,t.order_type,i.integral,i.time,i.property,i.integrallistID')
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $city_model = new \app\admin\model\City();
            foreach ($list as $key => $value){
                $list[$key]['order_num'] = $value['order_num'];
                if ($value['order_type'] == 1){
                    $list[$key]['order_type'] = '团购';
                }elseif ($value['order_type'] == 2){
                    $list[$key]['order_type'] = '买单';
                }
                if ($value['property'] == 4)
                    $list[$key]['order_type'] = '退款扣除积分';

                $list[$key]['area_id'] = $value['area_id'];
                $list[$key]['city_id'] = $value['city_id'];
                $list[$key]['province_id'] = $value['province_id'];

                //地区查询
                $province_id = $value['province_id'];
                $city_id = $value['city_id'];
                $area_id = $value['area_id'];
                if ($province_id){
                    $city1 = $city_model->where('cityID',$province_id)->value('name');
                    if ($city_id){
                        $city2 = $city_model->where('cityID',$city_id)->value('name');
                        if ($area_id){
                            $city3 = $city_model->where('cityID',$area_id)->value('name');
                            $list[$key]['city'] = $city1.'-'.$city2.'-'.$city3;
                        }else{
                            $list[$key]['city'] = $city1.'-'.$city2;
                        }
                    }else{
                        $list[$key]['city'] = $city1;
                    }
                }
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        $total = $this->model
            ->where(['type'=>1])
            ->where('property','in',[3,4])
            ->sum('integral');
        $this->view->assign("total", $total);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $order_id = $this->model->where('integrallistID','=',$ids)->value('id');
        $where = [
            'order_id' => $order_id,
            'pay_status'=>2,
            'tuan_type' => ['neq',2]
        ];
        $order_model = new TuanOrder();
        $row = $order_model
            ->with(['shop','user','selearnings'])
            ->where($where)
            ->find();

        //查找商家的会员信息，商家的地区
        $user_id = $row['shop']['user_id'];
        $user_model = new  \app\admin\model\User();
        $shop_user_info = $user_model->field('phone,account')->where(['userID'=>$user_id])->find();
        $province = $row['shop']['province_id'];
        $city = $row['shop']['city_id'];
        $area = $row['shop']['area_id'];
        $city_map = [
            'cityID'=>['in',$province.','.$city.','.$area]
        ];
        $city_model = new \app\admin\model\City();
        $local_name = $city_model->where($city_map)->column('name');
        $local_name = implode('-',$local_name);

        //计算当前税率

//        $row['rate'] = @number_format(($row['price'] - $row['settlement_price'])/$row['price'],2) * 100;

        $row['rate'] = @number_format(($row['price'] - $row['back_money'] - $row['settlement_price'])/$row['price'],2) * 100;


        $order_type = $this->order_type;
        $pay_type = $this->pay_type;
        $row['order_type_text'] = $order_type[$row['order_type']];
        $row['pay_type_text'] = $pay_type[$row['pay_type']];
        $row['shop']['local_name'] = $local_name;
        $row['shop']['user_info'] = $shop_user_info;
        $row['add_integral'] =  $row['order_status'] == 3 ? '+ '.(floor($row['price']/100)) : '未评价';

        if($row['discount'] == 0 ){
            $row['discount'] = 10;
        }
        $discount_price = $row['total_price'] * (1- $row['discount']/10);
        $row['discount_text'] = $row['discount'].'折';
        $row['discount'] = price_format($discount_price);
        $row['price'] = $row['price']/100;
        $row['total_price'] = $row['total_price']/100;
        $row['settlement_price'] = $row['settlement_price']/100;
        //查询消券时间
        if ($row['order_type'] == 1){
            $row['update_time'] = Db::name('tuan_code')->where('order_id','=',$row['order_id'])->where('is_used','=',1)->order('used_time desc')->value('used_time');
        }
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
