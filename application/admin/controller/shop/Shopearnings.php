<?php

namespace app\admin\controller\shop;

use app\admin\model\Property;
use app\admin\model\TuanOrder;
use app\admin\model\User;
use app\common\controller\Backend;
use think\Db;
use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Shopearnings extends Backend
{
    
    /**
     * Property模型对象
     */
    protected $model = null;
    protected $searchFields = 'user.phone,shop_name,user.account';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Shop');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function index()
    {
        $info = $this->request->param();
        list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $where_arr = get_object_vars(json_decode($info['filter']));
            $s_time = 0;
            $e_time = time();
            if (!empty($where_arr['time'])){
                $time = explode(' - ',$where_arr['time']);
                if ($time[0]){
                    $s_time = strtotime($time[0]);
                }
                if ($time[1]){
                    $e_time = strtotime($time[1]);
                }

            }
            $type = 1;
            if (count($where_arr)>0){
                //dump($where_arr);die;
                if (!empty($where_arr['province_id'])){
                    $province_id = $where_arr['province_id'];
                    if (!empty($where_arr['city_id'])){
                        $type = 2;
                        $city_id = $where_arr['city_id'];
                        $list = Db::query("select sum(s_p)/100 as spp,city_id ,c.name from ( select sum(price+subtract+allowance) as s_p,shop_id,(select area_id from dy_shop where dy_shop.shop_id = dy_tuan_order.shop_id and dy_shop.city_id=".$city_id.") as city_id from dy_tuan_order  
Where pay_status = 2 and  order_type <>3 and pay_time BETWEEN ".$s_time." AND ".$e_time."  group by shop_id ) as aaa,dy_city as c
Where aaa.city_id = c.cityID and c.PID = ".$city_id."
GROUP BY city_id ORDER BY spp desc");
                    }else{
                        $list = Db::query("select sum(s_p)/100 as spp,city_id ,c.name from
 ( select sum(price+subtract+allowance) as s_p,shop_id,(select city_id from dy_shop where dy_shop.shop_id = dy_tuan_order.shop_id) as city_id from dy_tuan_order  
Where pay_status = 2 and  order_type <>3 and pay_time BETWEEN ".$s_time." AND ".$e_time." 
 group by shop_id ) as aaa,dy_city as c
Where aaa.city_id = c.cityID and c.PID = ".$province_id."
GROUP BY city_id ORDER BY spp desc");
                    }
                }else{
                    $list = Db::query("
select sum(s_p)/100 as spp,city_id ,c.name from
 ( select sum(price+subtract+allowance) as s_p,shop_id,
 (select city_id from dy_shop where dy_shop.shop_id = dy_tuan_order.shop_id)
  as city_id from dy_tuan_order Where pay_status = 2 and  order_type <>3 and 
  pay_time BETWEEN ".$s_time." AND ".$e_time."  group by shop_id ) as aaa,dy_city as c
Where aaa.city_id = c.cityID GROUP BY city_id ORDER BY spp desc 
");
                }
            }else{
                $list = Db::query("
select sum(s_p)/100 as spp,city_id ,c.name from
 ( select sum(price+subtract+allowance) as s_p,shop_id,(select city_id from dy_shop where dy_shop.shop_id = dy_tuan_order.shop_id) as city_id from dy_tuan_order Where pay_status = 2 and  order_type <>3 group by shop_id ) as aaa,dy_city as c Where aaa.city_id = c.cityID GROUP BY city_id ORDER BY spp desc");
            }
            $total = count($list);
            if ($total>0){
                foreach ($list as $key=>$value){
                    $list[$key]['ranking'] = $key+1;
                    $where_phone['city_id'] = $value['city_id'];
                    $where_phone['type'] = $type;
                    $list[$key]['phone'] = Db::name('branch_office')->where($where_phone)->value('phone')??'暂未开通';
                    $list[$key]['type'] = $type;
                    $list[$key]['spp'] = sprintf("%.2f",$value['spp']);
                }
            }
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
}
