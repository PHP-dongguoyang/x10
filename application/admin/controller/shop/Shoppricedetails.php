<?php

namespace app\admin\controller\shop;

use app\admin\model\Admin;
use app\admin\model\AdminH;
use app\admin\model\Autonym;
use app\admin\model\Card;
use app\admin\model\Consume;
use app\admin\model\Property;
use app\admin\model\User;
use app\common\controller\Backend;

use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon tags
 */
class Shoppricedetails extends Backend
{
    
    /**
     * Tags模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('depositlist');

    }
    public function index()
    {
        list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        $this->request->filter(['strip_tags']);

        //var_dump($where);die;
        //$field = 'order_id,num,pay_status,order_num,order_type,price,pay_type,update_time,create_time,pay_time,delete,order_status';
        if ($this->request->isAjax()) {
            $consume_model = new Consume();
            $property_model = new Property();
//            return json($where);
            $total = $this->model
                ->where($where)
                ->where(['status'=>3])
                ->order($sort, $order)
                ->count();
            $list = $this->model
                ->where($where)
                ->where(['status'=>3])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
//            return $list;
            foreach ($list as $k=>$value){
                //提现金额
                $list[$k]['return_money'] = $consume_model->where('consumeID','=',$value['consumeID'])->value('fees');
                $list[$k]['serial_number'] = $consume_model->where('consumeID','=',$value['consumeID'])->value('ordernum');
                $list[$k]['return_money'] = $list[$k]['return_money']/100;
                //提现后的余额
//                $list[$k]['balance'] = $property_model->where(['userID'=>$value['userID'],'type'=>1])->value('value');
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    public function edit($ids = NULL)
    {
//        dump($ids);die();
        $where = [
            'depositlistID' => $ids,
            'status' => 3
        ];
        $row = $this->model
            ->where($where)
            ->find();
//        return \GuzzleHttp\json_encode($row);
        $user_model = new User();
        $consume_model = new Consume();
        $property_model = new Property();
        $autonym_model = new Autonym();
        $card_model = new Card();
        $adminh_model = new AdminH();
        $where1 = [
            'userID'=>$row['userID']
        ];
        $where2 = [
            'userID'=>$row['userID'],
            'type'=>1
        ];
        $where3 = [
            'consumeID'=>$row['consumeID'],
            'type'=>2
        ];
        $row['ordernum']  = $consume_model->where($where3)->value('ordernum');
        $row['account'] = $user_model->where($where1)->value('account') ;//用户id
        $row['balance'] = $property_model->where($where2)->value('value');//可提现金额
        $row['balance'] = ($row['balance']/100);
        $row['return_price'] = $consume_model->where($where3)->value('fees');//申请提现的金额
        $row['return_price'] = ($row['return_price']/100);
        $row['return_money'] =  $row['return_price']+$row['balance'];//本次交易前的账户余额
        $row['real_name'] = $autonym_model->where($where1)->value('name');//真实姓名
        $row['real_card'] = $autonym_model->where($where1)->value('indentity_card');//身份证
        $row['blank_card'] = $card_model->where($where1)->value('number');//银行卡号
        $row['bank'] = $card_model->where($where1)->value('bank');//银行
        $row['branch_bank'] = $card_model->where($where1)->value('branch_bank');//银行支行
        $row['admin_name'] = $adminh_model->where(['admin_id'=>$row['id']])->value('user_name');//审核人
        if (empty($row['admin_name'])){
            $row['admin_name'] = 'admin';
        }
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
