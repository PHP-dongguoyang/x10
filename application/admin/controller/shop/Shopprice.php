<?php

namespace app\admin\controller\shop;

use app\admin\model\Property;
use app\admin\model\TuanOrder;
use app\admin\model\User;
use app\common\controller\Backend;
use think\Db;
use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Shopprice extends Backend
{
    
    /**
     * Property模型对象
     */
    protected $model = null;
    protected $searchFields = 'user.phone,shop_name,user.account';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Shop');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function index()
    {
        list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        $this->request->filter(['strip_tags']);

        if ($this->request->isAjax()) {
            $total = $this->model
                ->with(['user'])
                ->where('closed','=',1)
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['user' => function ($query) {
                    $query->withField('phone,account');
                }])
                ->where($where)
                ->where('closed','=',1)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $city_model = new \app\admin\model\City();
            $user_model = new TuanOrder();
            $consume_model = new \app\admin\model\Consume();
            $property_model = new Property();
            foreach ($list as $key => $value) {
                //返现金额
                $list[$key]['return_money'] = price_format($user_model->where(['shop_id' => $value['shop_id'],'pay_status'=>2])->sum('back_money'));
                //账户余额
                $list[$key]['balance'] = price_format($property_model->where(['userID' => $value['user_id'], 'type' => 1])->value('value'));
                //店铺累计收益
                $list[$key]['grand_total'] = price_format($consume_model::where(['userID'=>$value['user_id'],'type'=>1,'source'=>12])->sum('fees'));
                //提现金额
                $list[$key]['draw_money'] = DB::name('consume')
                    ->alias('c')
                    ->join('depositlist d','d.consumeID=c.consumeID')
                    ->where('c.userID','=',$value['user_id'])
                    ->where('d.status','=',3)
                    ->where('c.source','=',4)
                    ->sum('fees');

                $list[$key]['draw_money'] = price_format($list[$key]['draw_money']);
                if ($list[$key]['draw_money'] === null) {
                    $list[$key]['draw_money'] = 0.00;
                }
                //地区查询
                $province_id = $value['province_id'];
                $city_id = $value['city_id'];
                $area_id = $value['area_id'];
                if ($province_id) {
                    $city1 = $city_model->where('cityID', $province_id)->value('name');
                    if ($city_id) {
                        $city2 = $city_model->where('cityID', $city_id)->value('name');
                        if ($area_id) {
                            $city3 = $city_model->where('cityID', $area_id)->value('name');
                            $list[$key]['city'] = $city1 . '-' . $city2 . '-' . $city3;
                        } else {
                            $list[$key]['city'] = $city1 . '-' . $city2;
                        }
                    } else {
                        $list[$key]['city'] = $city1;
                    }
                }
                //行业
                $list[$key]['cate_name'] = Db::name('industry')->where('industryID',$value['cate_id'])->value('name');
                //绑定会员数
                $list[$key]['user_num'] = Db::name('user')
                    ->where([
                            'PID' => $value['user_id'],
                            'type' => 3,
                            'attestation' => 7,
                        ]
                    )->count();
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 一级行业列表
     */
    function cate_list(){
        $where = [
            'type'  =>  1,
            'switch'=>  2
        ];
        $list = Db::name('industry')->where($where)->order('orderby asc')->select();
        $searchlist = [];
        if($list){
            foreach ($list as $k=>$v){
                $searchlist[] = [
                    'id'    =>  $v['industryID'],
                    'name'  =>  $v['name']
                ];
            }
        }
        $data = ['searchlist' => $searchlist];
        $this->success('', null, $data);
    }
}
