<?php

/**
 * @ zuorenci
 * @ 2017-12-25
 */
namespace app\admin\controller\tuan;

use app\common\controller\Backend;

use function FastRoute\TestFixtures\empty_options_cached;
use think\Controller;
use think\Request;
use think\Db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Cashbackrecord extends Backend
{
    
    /**
     * TuanOrder模型对象
     */
    protected $model = null;
    protected $order_type;
    protected $pay_type;
    protected $back_status;
    protected $field = 'order_id,tuan_id,back_status,back_money,total_price,integral,num,pay_status,order_num,order_type,price,pay_type,update_time,create_time,pay_time,delete,order_status,shop_id,user_id';

    protected $order_status;
    protected $searchFields = 'shop.shop_name';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('TuanOrder');
        $this->order_type = [
            '1'=>'团购订单',
            '2'=>'优惠买单',
            '3'=>'充值',
        ];
        $this->pay_type = [
            '0'=>'未支付',
            '1'=>'余额支付',
            '2'=>'支付宝',
            '3'=>'微信支付',
        ];
        $this->order_status = [//1待使用 2待评价 3已评价 4已取消 默认为0
            '0'=>'未支付',
            '1'=>'待使用',
            '2'=>'待评价',
            '3'=>'已评价',
            '4'=>'已取消',
        ];
        $this->back_status = [//-1返还给商家失败,0不需要返现,1返现待领取,2会员已领取,3商家已领取
            '-1'=>'返还给商家失败',
            '0'=>'不需要返现',
            '1'=>'返现待领取',
            '2'=>'会员已领取',
            '3'=>'商家已领取',
        ];

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->field($this->field)
                    ->with('tuaninfo')
                    ->where($where)
                    ->where([
                        'back_status'  =>  ['in',[-1,1,2,3]],
                        //'shop_id'    =>  $ids,
                    ])
                    ->order($sort, $order)
                    ->count();
            $list = $this->model
                    ->field($this->field)
                    ->with(['tuaninfo'=>function($query){
                        $query->withField('title');
                    }])
                    ->where($where)
                    ->where([
                        'back_status'  =>  ['in',[-1,1,2,3]],
                        //'shop_id'    =>  $ids,
                    ])
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            $tuan_info_model = new \app\admin\model\TuanInfo();
            $consume_model = new \app\admin\model\Consume();

            $order_type = $this->order_type;
            $pay_type = $this->pay_type;
            $order_status = $this->order_status;
            $back_status = $this->back_status;
            foreach ($list as $key=>$value){
                $list[$key]['user_coupon'] = ($value['total_price']-$value['integral']*100-$value['price'])/100;
                $list[$key]['total_price'] =  $value['total_price']/100;
                $list[$key]['price'] = $value['price']/100;
                if($value['order_type']!=1){
                    $list[$key]['title'] ='-';
                }else{
                    $list[$key]['title'] = $tuan_info_model->where('tuan_id',$value['tuan_id'])->value('title');
                }
                /*switch ($value['back_status']){
                    case -1:
                    case 1:
                        $list[$key]['get_time'] = '-';
                        break;
                    case 2:
                    case 3:
                        $list[$key]['gee_time'] = $consume_model->where('ordernum',$value['order_num'])->value('tiem');
                    break;
                }*/
                $list[$key]['order_type'] = $order_type[$value['order_type']];
                $list[$key]['pay_type'] = $pay_type[$value['pay_type']];
                $list[$key]['order_status'] = $order_status[$value['order_status']];
                $list[$key]['back_status'] = $back_status[$value['back_status']];
                $list[$key]['back_money'] = $value['back_money']/100;
                $list[$key]['jm_price'] = $value['total_price']-$value['price'];


            }
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        //$row = $this->model->get($ids);
        $user_model = new \app\admin\model\User();
        $shop_model = new \app\admin\model\Shop();
        $order_type = $this->order_type;
        $pay_type = $this->pay_type;
        $order_status = $this->order_status;
        $row = $this->model
            ->where('order_id',$ids)
//            ->field($this->field)
            ->find();
        $row['order_type'] = $order_type[$row['order_type']];
        $row['pay_type'] = $pay_type[$row['pay_type']];
        $row['order_status'] = $order_status[$row['order_status']];
        $row['user_coupon'] = ($row['total_price']-$row['integral']*100-$row['price'])/100;
        $row['price'] = $row['price']/100;
        $row['back_money'] = $row['back_money']/100;
        $row['total_price'] = $row['total_price']/100;


        //用户信息
        $user = $user_model->where('userID',$row['user_id'])->field('account,phone,nickname')->find();
        $row['user'] = $user;
        //店铺信息
        $shop = $shop_model->where('shop_id',$row['shop_id'])->field('shop_name,province_id,city_id,area_id,rate,discount')->find();
        $shop_user = $user_model->where('userID',$row['user_id'])->field('account,phone')->find();
        $shop['account'] = $shop_user['account'];
        $shop['phone'] = $shop_user['phone'];
        $row['shop'] = $shop;
        //优惠买单折扣
        $row['discount'] = Db::name('shop_youhui')->where(['shop_id'=>$row['shop_id']])->value('discount');
        //返现状态
        $back_status = [1=>'待领取',2=>'已领取',3=>'已退还商家'];
        $row['back_status'] = $back_status[$row['back_status']];
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 变更
     * @internal
     */
    public function change()
    {
        $this->success();
    }

    /**
     * 搜索下拉列表
     */
    public function searchlist()
    {
        $result = $this->model->limit(10)->select();
        $searchlist = [];
        foreach ($result as $key => $value)
        {
            $searchlist[] = ['id' => $value['url'], 'name' => $value['url']];
        }
        $data = ['searchlist' => $searchlist];
        $this->success('', null, $data);
    }
}
