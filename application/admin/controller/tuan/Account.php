<?php

namespace app\admin\controller\tuan;

use app\admin\model\Depositlist;
use app\admin\model\TuanOrder;
use app\common\controller\Backend;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Account extends Backend
{
    
    /**
     * Consume模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('accounts');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 充值列表
     * User: fuhang
     * Email: 515934402@qq.com
     * Time: 2017/12/6 0006 下午 2:08
     * @return string|\think\response\Json
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //获取查询条件
            $filter = $this->request->get("filter", '');
            $filter = json_decode($filter, TRUE);
            $where1 = [];
            $where2 = [];
            if($filter){
                $dateArr = explode(' - ',$filter['date']);
                $start = strtotime($dateArr[0]);
                $end = strtotime($dateArr[1]);
                $where1 = ['pay_time'=>['between',[$start,$end]]];
                $where2 = ['time'=>['between',[$start,$end]]];
            }
            /*list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list_all = $this->model
                ->where($where)
                ->order($sort, $order)
                ->select();

            $accounts_total = 0;    //账目总金额
            $accounts_in = 0;    //入账
            $accounts_out = 0;    //出账
            $order_total = 0;    //订单总额
            $recharge_total = 0;    //后台充值
            $share_total = 0;    //第三方消费
            $deposit = 0;    //提现
            if($list_all){
                foreach($list_all as $k => $v){
                    $accounts_total += $v['accounts_total'];
                    $accounts_in += $v['accounts_in'];
                    $accounts_out += $v['accounts_out'];
                    $order_total += $v['order_total'];
                    $recharge_total += $v['recharge_total'];
                    $share_total += $v['share_total'];
                    $deposit += $v['deposit_total'];
                }
            }

            //根据条件是否获取今天的数据
            $today = date('Y-m-d');
            $flag = false;
            if(empty($filter)){
                $flag = true;
            }else{
                $arr = explode(' - ',$filter['date']);
                if($arr[1] >= $today)
                    $flag = true;

                if($arr[1] == '')
                    $flag = true;
            }

            if($flag){
                // 获取今日的信息
                $start = strtotime($today);
                $end = strtotime('+1 day',$start)-1;
                $list = Db::name('consume')->where('time','between',[$start,$end])->select();
                $value1 = 0;    //充值
                $value2 = 0;    //提现
                $value3 = 0;    //分润
                $value4 = 0;    //订单交易
                $value5 = 0;    //后台充值
                $value6 = 0;    //第三方消费
                $value7 = 0;    //退款

                $TuanOrder = new TuanOrder();

                if($list){
                    foreach($list as $k => $v){
                        //1.all充值 type = 1
                        //2.all提现 type = 2
                        //3.all分润 type = 1 && source = 7,8,9,10.11
                        //4.all订单交易 type = 3 && type= 7
                        if($v['type'] == 1) {
                            if($v['source'] >=1 && $v['source'] <=3){
                                $value1 += $v['fees'];
                                if($v['source']==1)
                                    $value5 += $v['fees'];
                            }elseif($v['source'] >= 7 && $v['source'] <= 11){
                                if($v['userID'] != 354096 && $v['userID'] != 354110 && $v['userID'] != 354112)  //剔出分润假数据
                                    $value3 += $v['fees'];
                            }elseif($v['source'] == 16) {
                                $value4 += $v['fees'];
                            }
                        }elseif($v['type'] == 2){
                            if (Db::name('depositlist')->where(['consumeID' => $v['consumeID'], 'status' => 3])->count()>0) {
                                $value2 += $v['fees'];
                            }
                        }elseif($v['type'] == 3) {
                            $value4 += $v['fees'];
                            if ($v['source'] == 2 || $v['source'] == 3)
                                $value6 += $v['fees'];
                        }elseif ($v['type'] == 7){
                            $order = $TuanOrder->where('order_id', $v['id'])->find();
                            $value4 += $v['fees']+$order['subtract']+$order['allowance'];
                            if ($v['source'] == 2 || $v['source'] == 3)
                                $value6 += $v['fees']+$order['subtract']+$order['allowance'];
                        }elseif($v['type'] == 8){
                            if($v['source'] == 2 || $v['source'] == 3)
                                $value7 += $v['fees'];
                        }

                    }
                }

                $accounts_total += $value1+$value6-$value2-$value5-$value7;
                $accounts_out += $value2+$value7;
                $accounts_in += $value1+$value6-$value5;
                $order_total += $value4;
                $recharge_total += $value1;
                $share_total += $value3;
            }

            //佣金
            $brokerage_list = Db::name('remittal_day')->field('all_cash_money')
                ->order('remittal_day_id desc')->limit(8)->select();
            $brokerage = 0;
            foreach ($brokerage_list as $k=>$v){
                $brokerage += $v['all_cash_money'];
            }
            $brokerage = bcdiv($brokerage,100,2);*/

            // 获取充值总额
            $recharge_total = (new TuanOrder())
                ->where($where1)
                ->where('pay_status',2)
                ->where('order_type',3)
                ->sum('price');

            // 获取分润总金额
            $share_total = Db::name('consume')
                ->where($where2)
                ->where('type',1)
                ->where('source','between',[7,11])
                ->where('userID','not in',[354096,354110,354112])
                ->sum('fees');

            // 获取订单总额
            $value = (new TuanOrder())
                ->where($where1)
                ->where('pay_status','in',[-1,-2,2])
                ->where('order_type','in',[1,2,4])
                ->where('pay_type','>',0)
                ->field('sum(price) s1,sum(subtract) s2,sum(allowance) s3')
                ->select();
            $order_total = $value[0]['s1']+$value[0]['s2']+$value[0]['s3'];

            $list = [
/*                [
                    'id'    => 1,
                    'name'  => '账目总金额（元）',
                    'title'  => '平台留账',
                    'value' => price_format($accounts_total),
                    'url'   => url('tuan/account'),
                    'class' => 'panel bg-blue',
                    'time'  => time(),
                    'words' =>  '实时',
                    'out'   =>  price_format($accounts_out),
                    'in'    =>  price_format($accounts_in)
                ],*/
                [
                    'id'    => 2,
                    'name'  => '订单交易总金额（元）',
                    'title'  => '交易总额',
                    'value' => price_format($order_total),
                    'url'   => url('tuan/ordertransactionrecord'),
                    'class' => 'panel bg-aqua-gradient',
                    'time'  => time(),
                    'words' =>  '实时'
                ],
                [
                    'id'    => 3,
                    'name'  => '充值总金额（元）',
                    'title'  => '第三方充值',
                    'value' => price_format($recharge_total),
                    'url'   => url('tuan/tuanorder'),
                    'class' => 'panel bg-purple-gradient',
                    'time'  => time(),
                    'words' =>  '实时'
                ],
                [
                    'id'    => 4,
                    'name'  => '分润总金额（元）',
                    'title'  => '分润总额',
                    'value' => price_format($share_total),
                    'url'   => url('tuan/selearnings'),
                    'class' => 'panel bg-green-gradient',
                    'time'  => time(),
                    'words' =>  '非实时'
                ],
//                [
//                    'id'    => 5,
//                    'name'  => '补贴佣金池（元）',
//                    'title'  => '当前金额',
//                    'value' => $brokerage,
//                    'url'   => '',
//                    'class' => 'panel bg-red-gradient',
//                    'time'  => time(),
//                    'words' =>  '非实时'
//                ],
//                [
//                    'id'    => 6,
//                    'name'  => '财务提现（元）',
//                    'title'  => '累计提现',
//                    'value' => price_format($deposit),
//                    'url'   => url('tuan/depositlist'),
//                    'class' => 'panel bg-yellow-gradient',
//                    'time'  => time(),
//                    'words' =>  '非实时'
//                ]

            ];
            $result = array("total" => 0, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

}
