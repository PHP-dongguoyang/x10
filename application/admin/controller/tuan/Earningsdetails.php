<?php

namespace app\admin\controller\tuan;

use app\common\controller\Backend;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Earningsdetails extends Backend
{
    
    /**
     * Attract模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Attract');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $info = $this->request->param();
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with('User')
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with('User')
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $k=>$v){
                $list[$k]['number'] = $offset + $k + 1;
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        //获取订单号，商家ID和消费会员ID
        $order_id = Db::name('selearnings')->where('selearningsID',$info['source'])->value('id');
        $order_info = Db::name('tuan_order')->where('order_id',$order_id)->find();
        $user_id = $order_info['user_id'];
        $shop_user_id = Db::name('shop')->where('shop_id',$order_info['shop_id'])->value('user_id');
        $account_info = Db::name('user')->where(['userID'=>['in',[$user_id,$shop_user_id]]])->column('account','userID');
        $data['num'] = $order_info['order_num'];
        $data['s'] = $account_info[$shop_user_id];
        if($user_id){
            $data['u'] = $account_info[$user_id];
        }else{
            $data['u'] = '扫码支付用户';
        }
        $this->assign('info',$data);
        return $this->view->fetch();
    }
}
