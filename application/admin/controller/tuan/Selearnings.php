<?php

namespace app\admin\controller\tuan;

use app\common\controller\Backend;

use think\Controller;
use think\Request;
use think\Db;
use app\admin\model;

/**
 * 卖家市场收益缴费
 *
 * @icon fa fa-circle-o
 */
class Selearnings extends Backend
{
    
    /**
     * Selearnings模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Selearnings');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = Db::name('selearnings')
                ->alias('s')
                ->join('dy_tuan_order o','s.id = o.order_id','left')
                ->join('dy_shop sp','o.shop_id = sp.shop_id','left')
                ->where($where)
                ->where(['s.userID'=>['not in','354096,354110,354112']])    //剔除分润假数剧
                ->order($sort, $order)
                ->count();

            $list = Db::name('selearnings')
                ->alias('s')
                ->join('dy_tuan_order o','s.id = o.order_id','left')
                ->join('dy_shop sp','o.shop_id = sp.shop_id','left')
                ->field('s.selearningsID,o.order_id,o.order_num,s.origin,s.fees,o.pay_type,sp.province_id,sp.city_id,sp.area_id,s.id,s.time')
                ->where($where)
                ->where(['s.userID'=>['not in','354096,354110,354112']])    //剔除分润假数剧
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $selearnings_model = new model\Selearnings();
            foreach ($list as $k=>$v){
                //获取5种收益的和
                $attract = Db::name('attract')->where('source',$v['selearningsID'])->select();
                for ($i=1;$i<6;$i++){
                    $list[$k]['source'.($i+6)] = 0;
                }
                foreach ($attract as $key=>$val){
                    switch ($val['type']){
                        case 1:
                            $k1 = 'source8';break;
                        case 2:
                            $k1 = 'source10';break;
                        case 3:
                            $k1 = 'source9';break;
                        case 4:
                            $k1 = 'source7';break;
                        default :
                            $k1 = 'source11';break;
                    }
                    $list[$k][$k1] += $val['fees'];
                }
                //获取分给平台的收益
                $list[$k]['system'] = Db::name('systemer')->where('id',$v['selearningsID'])->value('fees');
                //获取省市区
                $list[$k]['pca'] = $selearnings_model->getPCA([$v['province_id'],$v['city_id'],$v['area_id']]);
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        //平台累积收益
        $earnings = Db::name('systemer')->sum('fees');
        $this->assign('earnings',price_format($earnings));
        return $this->view->fetch();
    }

    /**
     * 省市区
     */
    public function citylist()
    {
        $info = $this->request->param();
        $where = [
            'is_open'   =>  2
        ];
        if(!empty($info['PID'])){
            $where['PID'] = $info['PID'];
        }else{
            return json(['code'=>1,'data'=>[]]);
        }
        $list = Db::name('city')->where($where)->select();
        $data = [];
        foreach ($list as $k=>$v){
            $data[] = [
                'value' =>  $v['cityID'],
                'name'  =>  $v['name']
            ];
        }
        return json(['code'=>1,'data'=>$data]);
    }


}
