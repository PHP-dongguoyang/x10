<?php

namespace app\admin\controller\tuan;

use app\common\controller\Backend;

use think\Controller;
use think\Request;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Accountcheck extends Backend
{
    
    /**
     * AccountCheck模型对象
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('AccountCheck');

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * Notes:列表
     * User: "LiJinGuo"
     * Date: 2018/7/25
     * Time: 20:28
     * @return string|\think\response\Json
     * @throws \think\Exception
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $key => $value) {
                $list[$key]['saleroom'] = bcdiv($value['saleroom'], 100, 2);
                $list[$key]['saleroom_third'] = bcdiv($value['saleroom_third'], 100, 2);
                $list[$key]['saleroom_tuanli'] = bcdiv($value['saleroom_tuanli'], 100, 2);
                $list[$key]['saleroom_cash'] = bcdiv($value['saleroom_cash'], 100, 2);
                $list[$key]['saleroom_b2c'] = bcdiv($value['saleroom_b2c'], 100, 2);
                $list[$key]['refund_amount_third'] = bcdiv($value['refund_amount_third'], 100, 2);
                $list[$key]['refund_amount_tuanli'] = bcdiv($value['refund_amount_tuanli'], 100, 2);
                $list[$key]['deposit'] = bcdiv($value['deposit'], 100, 2);
                $list[$key]['deposit_finish'] = bcdiv($value['deposit_finish'], 100, 2);
                $list[$key]['deposit_pending'] = bcdiv($value['deposit_pending'], 100, 2);
                $list[$key]['deposit_b2c'] = bcdiv($value['deposit_b2c'], 100, 2);
                $list[$key]['retention_value_shop'] = bcdiv($value['retention_value_shop'], 100, 2);
                $list[$key]['retention_value_user'] = bcdiv($value['retention_value_user'], 100, 2);
                $list[$key]['retention_value_branch'] = bcdiv($value['retention_value_branch'], 100, 2);
                $list[$key]['system_recharge'] = bcdiv($value['system_recharge'], 100, 2);
                $list[$key]['third_recharge'] = bcdiv($value['third_recharge'], 100, 2);
                $list[$key]['shop_profit'] = bcdiv($value['shop_profit'], 100, 2);
                $list[$key]['shop_profit_back'] = bcdiv($value['shop_profit_back'], 100, 2);
                $list[$key]['selearnings'] = bcdiv($value['selearnings'], 100, 2);
                $list[$key]['agent_attract'] = bcdiv($value['agent_attract'], 100, 2);
                $list[$key]['attract'] = bcdiv($value['attract'], 100, 2);
                $list[$key]['shop_attract'] = bcdiv($value['shop_attract'], 100, 2);
                $list[$key]['user_attract'] = bcdiv($value['user_attract'], 100, 2);
                $list[$key]['branch_attract'] = bcdiv($value['branch_attract'], 100, 2);
                $list[$key]['cash_pool_in'] = bcdiv($value['cash_pool_in'], 100, 2);
                $list[$key]['cash_pool_out'] = bcdiv($value['cash_pool_out'], 100, 2);
                $list[$key]['subtract'] = bcdiv($value['subtract'], 100, 2);
                $list[$key]['tuanli_subsidy'] = bcdiv($value['tuanli_subsidy'], 100, 2);
                $list[$key]['tuanli_value'] = bcdiv($value['tuanli_value'], 100, 2);
            }
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * Notes:查看
     * User: "LiJinGuo"
     * Date: 2018/7/25
     * Time: 20:28
     */
    public function edit($ids = 0)
    {
        $list = $this->model
            ->where('id','eq',$ids)
            ->find();

            $list['saleroom'] = bcdiv($list['saleroom'], 100, 2);
            $list['saleroom_third'] = bcdiv($list['saleroom_third'], 100, 2);
            $list['saleroom_tuanli'] = bcdiv($list['saleroom_tuanli'], 100, 2);
            $list['saleroom_cash'] = bcdiv($list['saleroom_cash'], 100, 2);
            $list['saleroom_b2c'] = bcdiv($list['saleroom_b2c'], 100, 2);
            $list['refund_amount_third'] = bcdiv($list['refund_amount_third'], 100, 2);
            $list['refund_amount_tuanli'] = bcdiv($list['refund_amount_tuanli'], 100, 2);
            $list['deposit'] = bcdiv($list['deposit'], 100, 2);
            $list['deposit_finish'] = bcdiv($list['deposit_finish'], 100, 2);
            $list['deposit_pending'] = bcdiv($list['deposit_pending'], 100, 2);
            $list['deposit_b2c'] = bcdiv($list['deposit_b2c'], 100, 2);
            $list['retention_value_shop'] = bcdiv($list['retention_value_shop'], 100, 2);
            $list['retention_value_user'] = bcdiv($list['retention_value_user'], 100, 2);
            $list['retention_value_branch'] = bcdiv($list['retention_value_branch'], 100, 2);
            $list['system_recharge'] = bcdiv($list['system_recharge'], 100, 2);
            $list['third_recharge'] = bcdiv($list['third_recharge'], 100, 2);
            $list['shop_profit'] = bcdiv($list['shop_profit'], 100, 2);
            $list['shop_profit_back'] = bcdiv($list['shop_profit_back'], 100, 2);
            $list['selearnings'] = bcdiv($list['selearnings'], 100, 2);
            $list['agent_attract'] = bcdiv($list['agent_attract'], 100, 2);
            $list['attract'] = bcdiv($list['attract'], 100, 2);
            $list['shop_attract'] = bcdiv($list['shop_attract'], 100, 2);
            $list['user_attract'] = bcdiv($list['user_attract'], 100, 2);
            $list['branch_attract'] = bcdiv($list['branch_attract'], 100, 2);
            $list['cash_pool_in'] = bcdiv($list['cash_pool_in'], 100, 2);
            $list['cash_pool_out'] = bcdiv($list['cash_pool_out'], 100, 2);
            $list['subtract'] = bcdiv($list['subtract'], 100, 2);
            $list['tuanli_value'] = bcdiv($list['tuanli_value'], 100, 2);

        $this->view->assign("row", $list);
        return $this->view->fetch();
    }
}
