<?php

namespace app\admin\controller\tuan;

use app\common\controller\Backend;
use app\admin\model\User;
use think\Controller;
use think\Request;

/**
 *
 *
 * @icon fa fa-circle-o
 */
class Tuanorder extends Backend
{

    /**
     * TuanOrder模型对象
     */
    protected $model = null;

    protected $pay_type = [
      '1' => '余额',
      '2' => '支付宝',
      '3' => '微信'
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Consume');

    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个方法
     * 因此在当前控制器中可不用编写增删改查的代码,如果需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 充值列表
     * User: fuhang
     * Email: 515934402@qq.com
     * Time: 2017/12/6 0006 下午 2:08
     * @return string|\think\response\Json
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $map = ['source'=>['in','1,2,3']];
            $total = $this->model
                ->with(['user','recharge'])
                ->where($where)
                ->where($map)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with([
                    'user',
                    'recharge'=>function($query){
                        $query->withField('pay_type,create_time,pay_time,update_time');
                    }
                ])
                ->where($where)
                ->where($map)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            if($list)
                foreach($list as $k => $v){
                    $list[$k]['fees'] = price_format($v['fees']); //充值金额/元
                    if(!empty($v['recharge']) && $v['recharge']['pay_type']){
                        $list[$k]['pay_type'] = $this->pay_type[$v['recharge']['pay_type']];
                    }else{
                        $list[$k]['pay_type'] = '后台充值';
                    }
                }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 查看详情
     */
    public function edit($ids = NULL)
    {
        $row = $this->model
            ->with('recharge')
            ->where(['consumeID'=>$ids])
            ->find();
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            if (!in_array($row[$this->dataLimitField], $adminIds))
            {
                $this->error(__('You have no permission'));
            }
        }
        $row['price'] = $row['fees']/100;
        if(empty($row['recharge']['pay_type'])){
            $row['pay_type'] = '后台充值';
        }else{
            $row['pay_type'] = $this->pay_type[$row['recharge']['pay_type']];
        }
        $row['account'] = model('User')::get($row['userID'])->getAttr('account');
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
